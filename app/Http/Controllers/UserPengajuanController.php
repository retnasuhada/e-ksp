<?php

namespace App\Http\Controllers;

use App\Models\Anggota;
use App\Models\JenisPinjaman;
use App\Models\Pengajuan;
use App\Models\User;

class UserPengajuanController extends Controller
{
    public function index()
    {
        /** @var User $user*/
        $user = User::query()->where('id', auth()->user()->id)->with('anggota')->first();
        // periksa apakan anggota memiliki pengajuan dengan status menunggu persetujuan
        if (!$user->anggota) {
            $jenisPinjaman = JenisPinjaman::get();
            return view('user.pengajuan.produk-pinjaman-view', ['user' => $user, 'jenisPinjaman' => $jenisPinjaman]);
        }
        $cek = Pengajuan::query()->where('anggota_id', $user->anggota->id)->count();
        if ($cek > 0) {
            $anggota = Anggota::query()->where('user_id', auth()->user()->id)->first();
            $query = Pengajuan::query()->with('jenis')->where('anggota_id', $anggota->id)->paginate(10);

            return view('user.pengajuan.index', ['data' => $query]);
        } else {
            $skore = $user->anggota->skore ?? 0;
            $countProduk = JenisPinjaman::query()->where('score_minimum', '<=', $skore)->count();
            if ($countProduk > 0) {
                $jenisPinjaman = JenisPinjaman::query()->where('score_minimum', '<=', $skore)->get();
            } else {
                $jenisPinjaman = null;
            }

            return view('user.pengajuan.produk-pinjaman', ['user' => $user, 'jenisPinjaman' => $jenisPinjaman]);
        }
    }

    public function ajukan(JenisPinjaman $jenisPinjaman)
    {
        $user = User::query()->where('id', auth()->user()->id)->with('anggota')->first();
        $pengajuan = new Pengajuan();
        $pengajuan->anggota_id = $user->anggota->id;
        $pengajuan->jenis_pinjaman_id = $jenisPinjaman->id;
        $pengajuan->jumlah = $jenisPinjaman->jumlah;
        $pengajuan->tenor = $jenisPinjaman->tenor;
        $pengajuan->jenis_tenor = $jenisPinjaman->jenis_tenor;
        $pengajuan->jumlah_angsur = $jenisPinjaman->jumlah_angsur;
        $pengajuan->angsuran = $jenisPinjaman->angsuran;
        $pengajuan->tanggal_pengajuan = date('Y-m-d');
        $pengajuan->status = 'menunggu persetujuan';
        $pengajuan->save();

        return redirect()->route('users.pengajuan.index')->with(['success' => 'Pengajuan Berhasil']);
    }

    public function detail($id)
    {
        $pengajuan = Pengajuan::query()->where('id', $id)->with('anggota', 'jenis')->first();
        return view('user.pengajuan.show', ['pengajuan' => $pengajuan]);
    }
}
