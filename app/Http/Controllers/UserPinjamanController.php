<?php

namespace App\Http\Controllers;

use App\Models\Anggota;
use App\Models\JenisPinjaman;
use App\Models\Pengajuan;
use App\Models\Tagihan;
use App\Models\Pinjaman;
use App\Models\User;

class UserPinjamanController extends Controller
{
    public function index()
    {
        $anggota = Anggota::query()->where('user_id', auth()->user()->id)->first();
        if ($anggota) {
            $query = Pinjaman::query()->where('anggota_id', $anggota->id)->paginate(10);
        } else {
            $query = null;
        }

        return view('user.pinjaman.index', ['data' => $query]);
    }

    public function ajukan(JenisPinjaman $jenisPinjaman)
    {
        $user = User::query()->where('id', auth()->user()->id)->with('anggota')->first();
        $pengajuan = new Pengajuan();
        $pengajuan->anggota_id = $user->anggota->id;
        $pengajuan->jenis_pinjaman_id = $jenisPinjaman->id;
        $pengajuan->jumlah = $jenisPinjaman->jumlah;
        $pengajuan->tenor = $jenisPinjaman->tenor;
        $pengajuan->jenis_tenor = $jenisPinjaman->jenis_tenor;
        $pengajuan->jumlah_angsur = $jenisPinjaman->jumlah_angsur;
        $pengajuan->angsuran = $jenisPinjaman->angsuran;
        $pengajuan->tanggal_pengajuan = date('Y-m-d');
        $pengajuan->status = 'menunggu persetujuan';
        $pengajuan->save();

        return redirect()->route('users.pinjaman.index')->with(['success' => 'Pengajuan Berhasil']);
    }

    public function detail($id)
    {
        $pinjaman = Pinjaman::query()->where('id', $id)->with('anggota')->first();
        $tagihan = Tagihan::query()->where('pinjaman_id', $pinjaman->id)->get();
        return view('user.pinjaman.show', ['pinjaman' => $pinjaman, 'tagihan' => $tagihan]);
    }
}
