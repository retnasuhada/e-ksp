<?php

namespace App\Http\Controllers\Admin;

use App\Models\JenisPinjaman;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JenisPinjamanController extends Controller
{
    public function index()
    {
        $data = JenisPinjaman::where('status', true)->paginate(10);
        return view('admin.jenis_pinjaman.index', ['data' => $data]);
    }

    public function create()
    {
        return view('admin.jenis_pinjaman.add');
    }

    public function store(Request $request)
    {
        $request->validate([
            'jenis_pinjaman' => 'required',
            'jumlah' => ['required', 'integer', 'max:99999999'],
            'tenor' => 'required',
            'jenis_tenor' => 'required',
            'jumlah_angsur' => 'required',
            'biaya_layanan' => 'required',
            'denda' => 'required',
            'bunga' => 'required ||between:0,99.99',
            'score_minimum' => ['required', 'integer', 'min:10'],
        ]);

        $jenisPinjaman = new JenisPinjaman();
        $jenisPinjaman->jenis_pinjaman = $request->jenis_pinjaman;
        $jenisPinjaman->jumlah = $request->jumlah;
        $jenisPinjaman->tenor = $request->tenor;
        $jenisPinjaman->bunga = $request->bunga;
        $jenisPinjaman->jenis_tenor = $request->jenis_tenor;
        $jenisPinjaman->jumlah_angsur = $request->jumlah_angsur;
        $jenisPinjaman->biaya_layanan = $request->biaya_layanan;
        $jenisPinjaman->denda = $request->denda;
        $jenisPinjaman->score_minimum = $request->score_minimum;
        $jenisPinjaman->save();
        $jenisPinjaman->countAngsuran();

        return redirect(route('admin.jenisPinjaman.index'))->with(['success' => 'Jenis Pinjaman berhasil ditambah']);
    }

    public function show(JenisPinjaman $jenisPinjaman)
    {
        $jenisPinjaman = JenisPinjaman::where('id', $jenisPinjaman->id)->first();
        return view('admin.jenis_pinjaman.show', ['data' => $jenisPinjaman]);
    }

    public function edit(JenisPinjaman $jenisPinjaman)
    {
        $data = JenisPinjaman::where('id', $jenisPinjaman->id)->first();
        return view('admin.jenis_pinjaman.edit', ['data' => $data]);
    }

    public function update(JenisPinjaman $jenisPinjaman, Request $request)
    {
        $request->validate([
            'jenis_pinjaman' => 'required',
            'jumlah' => ['required', 'integer', 'max:99999999'],
            'tenor' => 'required',
            'jenis_tenor' => 'required',
            'jumlah_angsur' => 'required',
            'biaya_layanan' => 'required',
            'denda' => 'required',
            'bunga' => 'required ||between:0,99.99',
            'score_minimum' => ['required', 'integer', 'min:10'],
        ]);

        $jenisPinjaman->jenis_pinjaman = $request->jenis_pinjaman;
        $jenisPinjaman->jumlah = $request->jumlah;
        $jenisPinjaman->tenor = $request->tenor;
        $jenisPinjaman->jenis_tenor = $request->jenis_tenor;
        $jenisPinjaman->jumlah_angsur = $request->jumlah_angsur;
        $jenisPinjaman->biaya_layanan = $request->biaya_layanan;
        $jenisPinjaman->denda = $request->denda;
        $jenisPinjaman->bunga = $request->bunga;
        $jenisPinjaman->score_minimum = $request->score_minimum;
        $jenisPinjaman->update();
        $jenisPinjaman->countAngsuran();

        return redirect(route('admin.jenisPinjaman.index'))->with(['success' => 'Jenis Pinjaman berhasil diubah']);
    }
}
