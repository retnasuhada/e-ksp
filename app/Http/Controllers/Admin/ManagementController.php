<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ManagementController extends Controller
{
    public function index()
    {
        $users = User::where(['role' => 'admin'])->get();

        return view('admin.managements.index', ['users' => $users]);
    }

    public function create()
    {
        $roles = config('role');

        $data['roles'] = $roles;

        return view('admin.managements.create', ['roles' => $data]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|string|email|unique:users,email',
            'phone' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'dob' => 'sometimes|date',
            'role' => 'required|string',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'avatar' => 'sometimes|file|mimes:jpg,jpeg,png,gif|max:1024',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->gender = $request->gender;
        $user->role = $request->role;
        $user->email_verified_at = now();
        if ($request->has('dob')) {
            $user->dob = $request->dob;
        }

        $fileName = null;
        if ($request->hasFile('avatar')) {
            $original_filename      = $request->file('avatar')->getClientOriginalName();
            $original_filename_arr  = explode('.', $original_filename);
            $file_ext               = end($original_filename_arr);
            $destination_path       = storage_path('app/public/images/avatar');
            $image                  = (string) Str::uuid() . '.' . $file_ext;
            if ($request->file('avatar')->move($destination_path, $image)) {
                $fileName = 'storage/images/avatar/' . $image;
            }
            $user->avatar = $fileName;
        }

        $user->save();

        return redirect(route('admin.managements.index'))->with(['success' => 'Admin berhasil ditambah']);
    }

    public function edit(User $user)
    {
        return view('admin.managements.edit', ['user' => $user]);
    }

    public function update(User $user, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'dob' => 'sometimes|date',
            'avatar' => 'sometimes|file|mimes:jpg,jpeg,png,gif|max:1024',
        ]);

        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->gender = $request->gender;
        if ($request->has('dob')) {
            $user->dob = $request->dob;
        }

        $fileName = null;
        if ($request->hasFile('avatar')) {
            $original_filename      = $request->file('avatar')->getClientOriginalName();
            $original_filename_arr  = explode('.', $original_filename);
            $file_ext               = end($original_filename_arr);
            $destination_path       = storage_path('app/public/images/avatar');
            $image                  = (string) Str::uuid() . '.' . $file_ext;
            if ($request->file('avatar')->move($destination_path, $image)) {
                $fileName = 'storage/images/avatar/' . $image;
            }
            $user->avatar = $fileName;
        }

        $user->save();

        return redirect(route('admin.managements.index'))->with(['success' => 'Admin berhasil diubah']);
    }

    public function changePassword(User $user)
    {
        return view('admin.managements.change-password', ['user' => $user]);
    }

    public function updatePassword(User $user, Request $request)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user->update([
            'password' => Hash::make($request->password),
        ]);

        return back()->with('success', 'Password berhasil diubah');
    }
}
