<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tagihan;
use App\Models\Pinjaman;
use App\Models\Pengajuan;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;

class PinjamanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {
            $query = Pinjaman::from('pinjaman as a')
                ->select('a.*', 'c.name')
                ->join("anggota as b", "a.anggota_id", "=", "b.id")
                ->join("users as c", "c.id", "=", "b.user_id")
                ->where("c.name", 'like', "%{$request->search}%")
                ->orderBy("id", "desc")
                ->paginate(10);
        } else {
            $query = Pinjaman::from('pinjaman as a')
                ->select('a.*', 'c.name')
                ->join("anggota as b", "a.anggota_id", "=", "b.id")
                ->join("users as c", "c.id", "=", "b.user_id")->orderBy("id", "desc")
                ->paginate(10);
        }
        return view('admin.pinjaman.index', ['data' => $query]);
    }

    public function approved(Pengajuan $pengajuan)
    {
        $pengajuan->status = 'disetujui';
        $pengajuan->update();
        $pengajuan->initPinjaman();

        return redirect(route('admin.pengajuan.index'))->with(['success' => 'Pengajuan Berhasil Di Setujui']);
    }

    public function reject(Pengajuan $pengajuan)
    {
        $pengajuan->status = 'ditolak';
        $pengajuan->update();
        return redirect(route('admin.pengajuan.index'))->with(['success' => 'Pengajuan Berhasil Di Tolak']);
    }

    public function show($id)
    {
        $pinjaman = Pinjaman::query()->where('id', $id)->with('anggota')->first();
        $tagihan = Tagihan::query()->where('pinjaman_id', $pinjaman->id)->get();
        return view('admin.pinjaman.show', ['pinjaman' => $pinjaman, 'tagihan' => $tagihan]);
    }

    public function bayar($id)
    {
        $tagihan = Tagihan::query()->with('pinjaman')->where('id', $id)->first();
        $jatuh_tempo = $tagihan->jatuh_tempo;

        $date = Carbon::parse($jatuh_tempo . ' 23:00:00');
        $now = Carbon::now();
        $jumlah_denda = 0;
        if ($date < $now) {
            $denda = $tagihan->pinjaman->jenisPinjaman->denda;
            $diff = $date->diffInDays($now);
            $jumlah_denda = $tagihan->angsuran * $denda * $diff;
        } else {
            $diff = 0;
        }

        $total = $tagihan->angsuran + $jumlah_denda;
        $data = [
            'tagihan' => $tagihan,
            'denda' => $jumlah_denda,
            'keterlambatan' => $diff,
            'total' => $total
        ];

        return view('admin.pembayaran.form', ['data' => $data]);
    }
}
