<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pengajuan;
use App\Http\Controllers\Controller;
use App\Models\Tagihan;

class PengajuanPinjamanController extends Controller
{
    public function index()
    {
        $data = Pengajuan::with('anggota', 'jenis')->orderBy('id', 'DESC')->paginate(10);

        return view('admin.pengajuan.index', compact('data'));
    }

    public function approved(Pengajuan $pengajuan)
    {
        $pengajuan->status = 'disetujui';
        $pengajuan->update();
        $pengajuan->initPinjaman();

        return redirect(route('admin.pengajuan.index'))->with(['success' => 'Pengajuan Berhasil Di Setujui']);
    }

    public function reject(Pengajuan $pengajuan)
    {
        $pengajuan->status = 'ditolak';
        $pengajuan->update();
        return redirect(route('admin.pengajuan.index'))->with(['success' => 'Pengajuan Berhasil Di Tolak']);
    }

    public function show($id)
    {
        $pinjaman = Pengajuan::query()->where('id', $id)->with('anggota')->first();
        $tagihan = Tagihan::query()->where('pinjaman_id', $pinjaman->id)->get();
        return view('admin.pengajuan.show', ['pengajuan' => $pinjaman, 'tagihan' => $tagihan]);
    }
}
