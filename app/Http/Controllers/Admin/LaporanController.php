<?php

namespace App\Http\Controllers\Admin;


use App\Models\Pinjaman;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Anggota;
use App\Models\Pengajuan;
use App\Models\Pembayaran;
use Rap2hpoutre\FastExcel\FastExcel;

class LaporanController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.laporan.index');
    }

    public function download(Request $request)
    {
        $start = date('Y-m-d h:i:d', strtotime($request->start));
        $end = date('Y-m-d h:i:d', strtotime($request->end));
        if ($request->jenis_laporan == '1') {
            $report = $this->laporanPengajuan($start, $end);
            $name = 'Laporan-Pengajuan';
        }
        if ($request->jenis_laporan == '2') {
            $report = $this->laporanPinjaman($start, $end);
            $name = 'Laporan-Pinjaman';
        }
        if ($request->jenis_laporan == '3') {
            $report = $this->laporanPembayaran($start, $end);
            $name = 'Laporan-Pembayaran';
        }
        if ($request->jenis_laporan == '4') {
            $report = $this->laporanAnggota($start, $end);
            $name = 'Laporan-Anggota';
        }


        return (new FastExcel($report))->download($name . '.xlsx');
    }

    public function laporanPengajuan($start, $end)
    {
        $response = array();
        $no = 0;
        $report = Pengajuan::query()->with('anggota', 'jenis')->whereBetween('created_at', [$start, $end])->get();
        foreach ($report as $key => $value) {
            $no++;
            $response[$key] = [
                'no' => $no,
                'nama' =>  $value->anggota->user->name,
                'produk/jenis' => $value->jenis->jenis_pinjaman,
                'jumlah pengajuan' => $value->jumlah,
                'tenor' => $value->tenor,
                'jenis_tenor' => $value->jenis_tenor,
                'jumlah_angsur' => $value->jumlah_angsur,
                'angsuran' => $value->angsuran,
                'tanggal pengajuan' => $value->tanggal_pengajuan,
                'status' => $value->status,
            ];
        }
        return $response;
    }


    public function laporanPinjaman($start, $end)
    {
        $response = array();
        $no = 0;
        $report = Pinjaman::query()->with('anggota', 'jenisPinjaman')->whereBetween('created_at', [$start, $end])->get();
        foreach ($report as $key => $value) {
            $no++;
            $lunas = 'Belum Lunas';
            if ($value->flag_lunas == '1') {
                $lunas = 'Lunas';
            }
            $response[$key] = [
                'no' => $no,
                'nama' =>  $value->anggota->user->name,
                'invoice' =>  $value->invoice,
                'tanggal' => date('Y-m-d', strtotime($value->created_at)),
                'produk/jenis' => $value->jenisPinjaman->jenis_pinjaman,
                'jumlah pinjmaan' => $value->jumlah_pinjaman,
                'biaya layanan' => $value->biaya_layanan,
                'bunga' => $value->bunga,
                'jumlah diterima' => $value->jumlah_diterima,
                'status' => $lunas,
            ];
        }
        return $response;
    }

    public function laporanPembayaran($start, $end)
    {
        $response = array();
        $no = 0;
        $report = Pembayaran::query()->with('tagihan')->whereBetween('created_at', [$start, $end])->get();
        foreach ($report as $key => $value) {
            $no++;

            $response[$key] = [
                'no' => $no,
                'nama' =>  $value->tagihan->pinjaman->anggota->user->name,
                'invoice' =>  $value->invoice,
                'tanggal' => $value->tanggal,
                'angsuran ke' => $value->tagihan->angsuran_ke,
                'angsuran' => $value->angsuran,
                'keterlambatan' => $value->keterlambatan,
                'denda' => $value->denda,
                'pokok' => $value->tagihan->pokok,
                'bunga' => $value->tagihan->bunga,
                'total' => $value->total,
            ];
        }
        return $response;
    }

    public function laporanAnggota($start, $end)
    {
        $response = array();
        $no = 0;
        $report = Anggota::query()->with('user')->whereBetween('created_at', [$start, $end])->get();

        foreach ($report as $key => $value) {
            $no++;
            $status = 'Menunggu Verifikasi';
            if ($value->verified == true) {
                $status = 'Anggota Aktif';
            }
            $response[$key] = [
                'no' => $no,
                'nama' =>  $value->user->name ?? 'undifine',
                'Status Keanggotaan' =>  $status,
                'tanggal bergabung' => date('Y-m-d', strtotime($value->created_at)),
                'jenis kelamin' =>  $value->user->gender ?? 'undifine',
                'pkerjaan' =>  $value->pekerjaan  ?? 'undifine',
                'Skor Kredit' =>  $value->skore  ?? 'undifine',
            ];
        }
        return $response;
    }
}
