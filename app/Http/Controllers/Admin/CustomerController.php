<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Support\Str;
use App\Models\Anggota;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    public function index()
    {
        $users = User::where(['role' => 'customer'])->with('anggota')->get();
        return view('admin.customers.index', ['users' => $users]);
    }

    public function create()
    {
        return view('admin.customers.add');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|string|email|unique:users,email',
            'phone' => 'required',
            'address' => 'required',
            'pekerjaan' => 'required',
            'gender' => 'required',
            'dob' => 'sometimes|date|nullable',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'avatar' => 'sometimes|file|mimes:jpg,jpeg,png,gif|max:1024|nullable',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->gender = $request->gender;
        $user->role = 'customer';
        $user->email_verified_at = now();
        if ($request->has('dob')) {
            $user->dob = $request->dob;
        }

        if ($request->has('pekerjaan')) {
            $anggota = Anggota::query()->where('user_id', $user->id)->first();
            if (!$anggota) {
                $anggota = new Anggota();
                $anggota->nomor_anggota = '-';
                $anggota->user_id = $user->id;
                $anggota->pekerjaan = $request->pekerjaan;
                $anggota->save();
            } else {
                $anggota->pekerjaan = $request->pekerjaan;
                $anggota->update();
            }
        }

        $fileName = null;
        if ($request->hasFile('avatar')) {
            $original_filename      = $request->file('avatar')->getClientOriginalName();
            $original_filename_arr  = explode('.', $original_filename);
            $file_ext               = end($original_filename_arr);
            $destination_path       = public_path('assets/img/avatar');
            $image                  = (string) Str::uuid() . '.' . $file_ext;
            if ($request->file('avatar')->move($destination_path, $image)) {
                $fileName = $image;
            }
            $user->avatar = $fileName;
        }

        $user->save();

        return redirect(route('admin.customers.index'))->with(['success' => 'Customer berhasil ditambah']);
    }

    public function show(User $user)
    {
        $anggota = Anggota::where('user_id', $user->id)->first();
        return view('admin.customers.show', ['user' => $user, 'anggota' => $anggota]);
    }

    public function approved(Anggota $anggota)
    {
        $anggota->nomor_anggota = $this->generateAnggota();
        $anggota->skore = 50;
        $anggota->verified = 1;
        $anggota->verified_at = now();
        $anggota->verified_by = auth()->user()->id;
        $anggota->update();

        return redirect(route('admin.customers.index'))->with(['success' => 'Anggota berhasil Di Setujui']);
    }

    public function reject(Anggota $anggota)
    {
        $anggota->nomor_anggota = $this->generateAnggota();
        $anggota->skore = 0;
        $anggota->verified = 2;
        $anggota->verified_at = now();
        $anggota->verified_by = auth()->user()->id;
        $anggota->update();
        return redirect(route('admin.customers.index'))->with(['success' => 'Anggota Di Tolak']);
    }

    public function verif(User $user)
    {
        $anggota = Anggota::where('user_id', $user->id)->first();
        return view('admin.customers.verif', ['user' => $user, 'anggota' => $anggota]);
    }

    public function edit(User $user)
    {
        $data = User::where('id', $user->id)->with('anggota')->first();
        return view('admin.customers.edit', ['user' => $data]);
    }

    public function update(User $user, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'dob' => 'sometimes|date|nullable',
            'avatar' => 'sometimes|file|mimes:jpg,jpeg,png,gif|max:1024|nullable',
        ]);

        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->gender = $request->gender;
        if ($request->has('dob')) {
            $user->dob = $request->dob;
        }

        $fileName = null;
        if ($request->hasFile('avatar')) {
            $original_filename      = $request->file('avatar')->getClientOriginalName();
            $original_filename_arr  = explode('.', $original_filename);
            $file_ext               = end($original_filename_arr);
            $destination_path       = public_path('assets/img/avatar');
            $image                  = (string) Str::uuid() . '.' . $file_ext;
            if ($request->file('avatar')->move($destination_path, $image)) {
                $fileName = $image;
            }
            $user->avatar = $fileName;
        }

        $user->save();

        return redirect(route('admin.customers.index'))->with(['success' => 'Customer berhasil diubah']);
    }


    public function generateAnggota()
    {
        $anggota = Anggota::max('nomor_anggota');

        if ($anggota == '-') {
            $no = 1;
        } else {
            $array = explode("/", $anggota);
            $no = $array[2] + 1;
        }

        $number_anggota = Date('Y') . '/' . $this->month(Date('m')) . '/' . $no;

        return $number_anggota;
    }

    public function month($m)
    {
        if ($m == '1') {
            $month = 'I';
        }
        if ($m == '2') {
            $month = 'II';
        }
        if ($m == '3') {
            $month = 'III';
        }
        if ($m == '4') {
            $month = 'IV';
        }
        if ($m == '5') {
            $month = 'V';
        }
        if ($m == '6') {
            $month = 'VI';
        }
        if ($m == '7') {
            $month = 'VII';
        }
        if ($m == '8') {
            $month = 'VIII';
        }
        if ($m == '9') {
            $month = 'IX';
        }
        if ($m == '10') {
            $month = 'X';
        }
        if ($m == '11') {
            $month = 'XI';
        }
        if ($m == '12') {
            $month = 'XII';
        }

        return $month;
    }
}
