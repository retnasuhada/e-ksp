<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PenggunaController extends Controller
{
    public function index()
    {
        $users = User::with('anggota')->get();
        return view('admin.users.index', ['users' => $users]);
    }

    public function create()
    {
        return view('admin.customers.add');
    }

    public function show($id)
    {
        $user = User::query()->where('id', $id)->first();

        return view('admin.users.show', ['user' => $user]);
    }

    public function update(Request $request)
    {

        $user = User::query()->where('id', $request->id)->first();
        $user->role = $request->role;
        $user->update();
        return redirect(route('admin.pengguna.index'))->with(['success' => 'Customer berhasil diubah']);
    }
}
