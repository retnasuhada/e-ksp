<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Anggota;

class AdminController extends Controller
{
    public function myProfile()
    {
        /** @var User $user*/
        $user = auth()->user();
        $anggota = Anggota::where('user_id', $user->id)->first();
        return view('admin.profile', ['user' => $user, 'anggota' => $anggota]);
    }

    public function editProfile()
    {
        $user = auth()->user();

        return view('admin.form-edit-profile', ['admin' => $user]);
    }

    public function update(Request $request)
    {
        /** @var User $user*/
        $user = auth()->user();

        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'address' => 'required',
            'gender' => 'required',
            'dob' => 'required',
            'avatar' => 'sometimes|file|mimes:jpg,jpeg,png,gif|max:1024',
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->gender = $request->gender;
        $user->dob = $request->dob;

        $fileName = null;
        if ($request->hasFile('avatar')) {
            $original_filename      = $request->file('avatar')->getClientOriginalName();
            $original_filename_arr  = explode('.', $original_filename);
            $file_ext               = end($original_filename_arr);
            $destination_path       = storage_path('app/public/images/avatar');
            $image                  = (string) Str::uuid() . '.' . $file_ext;
            if ($request->file('avatar')->move($destination_path, $image)) {
                $fileName = 'storage/images/avatar/' . $image;
            }
            $user->avatar = $fileName;
        }

        $user->save();

        return redirect(route('admin.myProfile'))->with(['success' => 'Profil berhasil diubah']);
    }

    public function changePassword()
    {
        return view('admin.change-password');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        /** @var User $user*/
        $user = auth()->user();

        $currentPassword = $user->password;
        $oldPassword = $request->old_password;

        if (Hash::check($oldPassword, $currentPassword)) {
            $user->update([
                'password' => Hash::make($request->password),
            ]);
            return back()->with('success', 'Password berhasil diubah');
        } else {
            return back()->with('error', 'Password lama tidak benar');
        }
    }
}
