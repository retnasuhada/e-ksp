<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pinjaman;
use App\Models\Pembayaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Anggota;
use App\Models\Tagihan;
use Illuminate\Support\Carbon;
use PDF;

class PembayaranController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {
            $query = Pembayaran::from('pembayaran as a')
                ->select('a.*', 'e.name', 'c.jumlah_pinjaman', 'b.angsuran_ke')
                ->join("tagihan as b", "a.tagihan_id", "=", "b.id")
                ->join("pinjaman as c", "c.id", "=", "b.pinjaman_id")
                ->join("anggota as d", "d.id", "=", "c.anggota_id")
                ->join("users as e", "e.id", "=", "d.user_id")
                ->where("e.name", 'like', "%{$request->search}%")
                ->orderBy("id", "desc")
                ->paginate(10);
        } else {
            $query = Pembayaran::from('pembayaran as a')
                ->select('a.*', 'e.name', 'c.jumlah_pinjaman', 'b.angsuran_ke')
                ->join("tagihan as b", "a.tagihan_id", "=", "b.id")
                ->join("pinjaman as c", "c.id", "=", "b.pinjaman_id")
                ->join("anggota as d", "d.id", "=", "c.anggota_id")
                ->join("users as e", "e.id", "=", "d.user_id")
                ->orderBy("id", "desc")
                ->paginate(10);
        }
        return view('admin.pembayaran.index', ['data' => $query]);
    }

    public function add()
    {
        return view('admin.pembayaran.add');
    }

    public function store(Request $request)
    {
        $tagihan = Tagihan::query()->with('pinjaman')->where('id', $request->tagihan_id)->first();
        $jatuh_tempo = $tagihan->jatuh_tempo;
        $date = Carbon::parse($jatuh_tempo . ' 23:00:00');
        $now = Carbon::now();
        $jumlah_denda = 0;
        if ($date < $now) {
            $denda = $tagihan->pinjaman->jenisPinjaman->denda;
            $diff = $date->diffInDays($now);
            $jumlah_denda = $tagihan->angsuran * $denda * $diff;
        } else {
            $diff = 0;
        }

        $total = $tagihan->angsuran + $jumlah_denda;

        $pembayaran = new Pembayaran;
        $pembayaran->tagihan_id = $tagihan->id;
        $pembayaran->tanggal =  $now->toDateString();
        $pembayaran->angsuran = $tagihan->angsuran;
        $pembayaran->keterlambatan = $diff;
        $pembayaran->denda = $jumlah_denda;
        $pembayaran->total = $total;
        $pembayaran->invoice = Pembayaran::quickRandom();
        $pembayaran->save();
        $tagihan->lunas = 1;
        $tagihan->update();

        // cek apakah tagihan sudah lunas atatu belum;
        $tagihaCek = Tagihan::query()->where(['lunas' => false, 'pinjaman_id' => $tagihan->pinjaman_id])->count();
        if ($tagihaCek == 0) {
            $pinjaman = Pinjaman::query()->where('id', $tagihan->pinjaman_id)->first();
            $pinjaman->flag_lunas = true;
            $pinjaman->update();

            // update skore kredit
            $anggota = Anggota::query()->where('id', $pinjaman->anggota_id)->first();
            $recortagihan = Tagihan::query()->where('pinjaman_id', $tagihan->pinjaman_id)->pluck('id');
            $recordPembayaran = Pembayaran::query()->whereIn('tagihan_id', $recortagihan)->sum('keterlambatan');
            if ($recordPembayaran > 0) {
                $anggota->skore = $anggota->skore - 10;
            } else {
                $anggota->skore = $anggota->skore + 10;
            }
            $anggota->update();
        }

        return redirect()->route('admin.pembayaran.index')->with(['success' => 'Pembayaran Berhasil']);
    }

    public function show($id)
    {
        $pembayaran = Pembayaran::query()->with('tagihan')->where('id', $id)->first();
        return view('admin.pembayaran.show', ['pembayaran' => $pembayaran]);
    }

    public function cetak($id)
    {
        $pembayaran = Pembayaran::query()->with('tagihan')->where('id', $id)->first();
        $pdf = PDF::loadView('admin.pembayaran.pdf', ['pembayaran' => $pembayaran]);
        return $pdf->download('invoice.pdf');
    }
}
