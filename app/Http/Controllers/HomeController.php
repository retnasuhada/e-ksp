<?php

namespace App\Http\Controllers;

use App\Models\Pembayaran;
use Illuminate\Http\Request;
use App\Models\Pinjaman;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user();
        if ($user->role == 'superadmin' || $user->role == 'admin') {
            $pinjaman = Pinjaman::query()->sum('jumlah_pinjaman');
            $biaya_layanan = Pinjaman::query()->sum('biaya_layanan');
            $pembayaran = Pembayaran::query()->sum('angsuran');
            $denda = Pembayaran::query()->sum('denda');
            $jumlah_peminjam = Pinjaman::query()->count();
            $data_pinjaman = Pinjaman::query()->with('anggota')->orderBy('jumlah_pinjaman', 'DESC')->limit(10)->get();
            $data = [
                'pinjaman' => $pinjaman,
                'pembayaran' => $pembayaran,
                'denda' => $denda,
                'biaya_layanan' => $biaya_layanan,
                'jumlah_peminjam' => $jumlah_peminjam,
                'data_pinjaman' => $data_pinjaman,
            ];
            return view('admin.dashboard', ['data' => $data]);
        } else {
            return redirect()->route('users.myProfile');
        }
    }
}
