<?php

namespace App\Http\Controllers;

use App\Models\Anggota;
use App\Models\JenisPinjaman;
use App\Models\Pengajuan;
use App\Models\User;
use App\Models\Pembayaran;

class UserPembayaranController extends Controller
{
    public function index()
    {
        $anggota = Anggota::query()->where('user_id', auth()->user()->id)->first();
        $query = null;
        if ($anggota) {
            $query = Pembayaran::from('pembayaran as a')
                ->select('a.*', 'e.name', 'c.jumlah_pinjaman', 'b.angsuran_ke')
                ->join("tagihan as b", "a.tagihan_id", "=", "b.id")
                ->join("pinjaman as c", "c.id", "=", "b.pinjaman_id")
                ->join("anggota as d", "d.id", "=", "c.anggota_id")
                ->join("users as e", "e.id", "=", "d.user_id")
                ->where("d.id", $anggota->id)
                ->orderBy("id", "desc")
                ->paginate(10);
        }

        return view('user.pembayaran.index', ['data' => $query]);
    }

    public function ajukan(JenisPinjaman $jenisPinjaman)
    {
        $user = User::query()->where('id', auth()->user()->id)->with('anggota')->first();
        $pengajuan = new Pengajuan();
        $pengajuan->anggota_id = $user->anggota->id;
        $pengajuan->jenis_pinjaman_id = $jenisPinjaman->id;
        $pengajuan->jumlah = $jenisPinjaman->jumlah;
        $pengajuan->tenor = $jenisPinjaman->tenor;
        $pengajuan->jenis_tenor = $jenisPinjaman->jenis_tenor;
        $pengajuan->jumlah_angsur = $jenisPinjaman->jumlah_angsur;
        $pengajuan->angsuran = $jenisPinjaman->angsuran;
        $pengajuan->tanggal_pengajuan = date('Y-m-d');
        $pengajuan->status = 'menunggu persetujuan';
        $pengajuan->save();

        return redirect()->route('users.pengajuan.index')->with(['success' => 'Pengajuan Berhasil']);
    }

    public function detail($id)
    {
        $pengajuan = Pengajuan::query()->where('id', $id)->with('anggota', 'jenis')->first();
        return view('user.pengajuan.show', ['pengajuan' => $pengajuan]);
    }
}
