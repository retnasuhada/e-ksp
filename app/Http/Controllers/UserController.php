<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Models\Anggota;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function myProfile()
    {
        /** @var User $user*/
        $user = auth()->user();
        $anggota = Anggota::where('user_id', $user->id)->first();
        return view('admin.users.profile', ['user' => $user, 'anggota' => $anggota]);
    }

    public function editProfile()
    {
        $user = User::query()->where('id', Auth::user()->id)->with('anggota')->first();
        return view('admin.users.edit-profile', ['user' => $user]);
    }

    public function update(Request $request)
    {
        /** @var User $user*/
        $user = auth()->user();

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'phone' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'dob' => 'sometimes|date|nullable',
            'avatar' => 'sometimes|file|mimes:jpg,jpeg,png,gif|max:1024|nullable',
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->gender = $request->gender;
        if ($request->has('dob')) {
            $user->dob = $request->dob;
        }

        if ($request->has('pekerjaan')) {
            $anggota = Anggota::query()->where('user_id', $user->id)->first();
            if (!$anggota) {
                $anggota = new Anggota();
                $anggota->nomor_anggota = '-';
                $anggota->user_id = $user->id;
                $anggota->pekerjaan = $request->pekerjaan;
                $anggota->save();
            } else {
                $anggota->pekerjaan = $request->pekerjaan;
                $anggota->update();
            }
        }

        $fileName = null;
        if ($request->hasFile('avatar')) {
            $original_filename      = $request->file('avatar')->getClientOriginalName();
            $original_filename_arr  = explode('.', $original_filename);
            $file_ext               = end($original_filename_arr);
            $destination_path       = public_path('assets/img/avatar');
            $image                  = (string) Str::uuid() . '.' . $file_ext;
            if ($request->file('avatar')->move($destination_path, $image)) {
                $fileName = $image;
            }
            $user->avatar = $fileName;
        }

        $user->save();

        return redirect(route('users.myProfile'))->with(['success' => 'Profil berhasil diubah']);
    }

    public function changePassword()
    {
        return view('admin.users.change-password');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        /** @var User $user*/
        $user = auth()->user();

        $currentPassword = $user->password;
        $oldPassword = $request->old_password;

        if (Hash::check($oldPassword, $currentPassword)) {
            $user->update([
                'password' => Hash::make($request->password),
            ]);
            return back()->with('success', 'Password berhasil diubah');
        } else {
            return back()->with('error', 'Password lama tidak benar');
        }
    }

    public function uploadIdentity()
    {
        return view('admin.users.identity.upload-ktp');
    }

    public function uploadIdentityStore(Request $request)
    {
        $request->validate([
            'ktp_number' => 'required|digits:16|string',
            'ktp_file' => 'required|file|mimes:jpg,png,jpg|max:1024'
        ]);

        /** @var User $user*/

        $user = auth()->user();

        $fileName = null;
        if ($request->hasFile('ktp_file')) {
            $original_filename      = $request->file('ktp_file')->getClientOriginalName();
            $original_filename_arr  = explode('.', $original_filename);
            $file_ext               = end($original_filename_arr);
            $destination_path       = public_path('assets/img/ktp');
            $image                  = (string) Str::uuid() . '.' . $file_ext;
            if ($request->file('ktp_file')->move($destination_path, $image)) {
                $fileName = $image;
            }
        }

        $params = [
            'nomor_ktp' => $request->ktp_number,
            'file_ktp' => $fileName,
        ];

        $check = Anggota::where('user_id', $user->id)->exists();
        if ($check) {
            Anggota::updateOrCreate(['user_id' => $user->id], $params);
        } else {
            $user->anggota()->create($params);
        }

        return redirect(route('users.myProfile'))->with(['success' => 'Ktp berhasil di upload']);
    }
}
