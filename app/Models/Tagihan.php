<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tagihan extends Model
{
    protected $table = 'tagihan';
    protected $fillable = [
        'pinjaman_id',
        'angsuran',
        'angsuran_ke',
        'jatuh_tempo',
        'pokok',
        'bunga',
        'lunas',
    ];

    public function pinjaman()
    {
        return $this->hasOne(Pinjaman::class, 'id', 'pinjaman_id')->with('jenisPinjaman', 'anggota', 'pengajuan');
    }
}
