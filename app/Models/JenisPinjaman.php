<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisPinjaman extends Model
{

    protected $table = 'jenis_pinjaman';
    protected $fillable = [
        'jenis_pinjaman',
        'jumlah',
        'biaya_layanan',
        'tenor',
        'jenis_tenor',
        'jumlah_angsur',
        'angsuran',
        'verified',
        'denda',
        'bunga',
        'score_minimum',
        'status',
    ];

    public function countAngsuran()
    {
        $pokok = $this->jumlah;
        $bunga = $this->bunga * $pokok / 100;
        $total = $pokok + $bunga;
        $angsuran = $this->pembulatan(round($total / $this->jumlah_angsur));
        $this->angsuran = $angsuran;
        $this->update();
    }

    public function pembulatan($uang)
    {
        $ratusan = substr($uang, -3);
        if ($ratusan < 500) {
            $akhir = $uang - $ratusan;
        } else {
            $akhir = $uang + (1000 - $ratusan);
        }
        return $akhir;
    }
}
