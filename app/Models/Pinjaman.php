<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pengajuan;

class Pinjaman extends Model
{
    protected $table = 'pinjaman';
    protected $fillable = [
        'jenis_pinjaman_id',
        'pengajuan_id',
        'invoice',
        'anggota_id',
        'jumlah_pinjaman',
        'biaya_layanan',
        'jumlah_diterima',
        'bunga',
        'flag_lunas',
    ];

    public function anggota()
    {
        return $this->hasOne(Anggota::class, 'id', 'anggota_id')->with('user');
    }

    public function pengajuan()
    {
        return $this->hasOne(Pengajuan::class, 'id', 'pengajuan_id');
    }

    public function jenisPinjaman()
    {
        return $this->hasOne(JenisPinjaman::class, 'id', 'jenis_pinjaman_id');
    }
}
