<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pengajuan;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';
    protected $fillable = [
        'tagihan_id',
        'tanggal',
        'angsuran',
        'keterlambatan',
        'denda',
        'total',
    ];

    public function tagihan()
    {
        return $this->hasOne(Tagihan::class, 'id', 'tagihan_id')->with('pinjaman');
    }

    public static function quickRandom($length = 12)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
}
