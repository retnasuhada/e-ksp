<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pinjaman;
use App\Models\Tagihan;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Pengajuan extends Model
{
    protected $table = 'pengajuan';
    protected $fillable = [
        'anggota_id',
        'jenis_pinjaman_id',
        'jumlah',
        'tenor',
        'jenis_tenor',
        'jumlah_angsur',
        'angsuran',
        'tanggal_pengajuan',
        'status',
    ];

    public function anggota()
    {
        return $this->hasOne(Anggota::class, 'id', 'anggota_id')->with('user');
    }

    public function jenis()
    {
        return $this->hasOne(JenisPinjaman::class, 'id', 'jenis_pinjaman_id');
    }

    public function initPinjaman()
    {
        DB::beginTransaction();
        try {
            $pinjaman = Pinjaman::query()->where('pengajuan_id', $this->id)->first();
            $jenis = JenisPinjaman::query()->where('id', $this->jenis_pinjaman_id)->first();
            $bunga = $this->jumlah * $jenis->bunga / 100;
            if (!$pinjaman) {
                $pinjaman = new Pinjaman;
                $pinjaman->jenis_pinjaman_id = $this->jenis_pinjaman_id;
                $pinjaman->pengajuan_id = $this->id;
                $pinjaman->invoice = $this->quickRandom();
                $pinjaman->anggota_id = $this->anggota_id;
                $pinjaman->bunga = $bunga;
                $pinjaman->jumlah_pinjaman = $this->jumlah;
                $pinjaman->biaya_layanan = $this->jenis->biaya_layanan;
                $pinjaman->jumlah_diterima = $this->jumlah - $this->jenis->biaya_layanan;
                $pinjaman->flag_lunas = false;
                $pinjaman->save();
            }
            $angsuran_bunga = $bunga / $this->jumlah_angsur;

            $now = Carbon::now();
            $newDateTime = Carbon::now()->addDay();
            $x = 0;
            for ($i = 0; $i < $this->jumlah_angsur; $i++) {
                // jika jenis tenor = hari
                if ($this->jenis_tenor == 'hari') {
                    $interval = round($this->tenor / $this->jumlah_angsur);
                }

                // jika jenis tenor = minggu
                if ($this->jenis_tenor == 'minggu') {
                    $jumlah_hari = $this->tenor * 7;
                    $interval = round($jumlah_hari / $this->jumlah_angsur);
                }

                // jika jenis tenor = bulan
                if ($this->jenis_tenor == 'bulan') {
                    $jumlah_hari = $this->tenor * 30;
                    $interval = round($jumlah_hari / $this->jumlah_angsur);
                }

                $x += $interval;
                $date = $now->addDay($x)->format('Y-m-d');
                $tagihan = new Tagihan;
                $tagihan->pinjaman_id = $pinjaman->id;
                $tagihan->angsuran = $this->angsuran;
                $tagihan->angsuran_ke = $i + 1;
                $tagihan->jatuh_tempo = $date;
                $tagihan->lunas = false;
                $tagihan->bunga = $angsuran_bunga;
                $tagihan->pokok = $this->angsuran - $angsuran_bunga;
                $tagihan->save();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    public static function quickRandom($length = 12)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
}
