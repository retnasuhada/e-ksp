<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserIdentity extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'ktp_number',
        'ktp_file',
    ];

    protected $casts = [
        'ktp_number' => 'string',
    ];
}
