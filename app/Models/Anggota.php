<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{

    protected $table = 'anggota';

    protected $fillable = [
        'nomor_anggota',
        'user_id',
        'nomor_ktp',
        'file_ktp',
        'skore',
        'pekerjaan',
        'verified',
        'verified_at',
        'verified_by',
    ];

    public function verified($verif)
    {
        $response = 'Menunggu Persetujuan';
        if ($verif == '1') {
            $response = 'Disetujui';
        }
        if ($verif == '2') {
            $response = 'Ditolak';
        }

        return $response;
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
