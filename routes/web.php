<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserPinjamanController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\PengajuanPinjamanController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\JenisPinjamanController;
use App\Http\Controllers\Admin\PinjamanController;
use App\Http\Controllers\Admin\PembayaranController;
use App\Http\Controllers\UserPengajuanController;
use App\Http\Controllers\UserPembayaranController;
use App\Http\Controllers\Admin\LaporanController;
use App\Http\Controllers\Admin\PenggunaController;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['verify' => true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/profile', [AdminController::class, 'myProfile'])->name('myProfile');
        Route::get('/profile-edit', [AdminController::class, 'editProfile'])->name('edit.profile');
        Route::put('/update', [AdminController::class, 'update'])->name('update.profile');
        Route::get('/change-password', [AdminController::class, 'changePassword'])->name('change.password');
        Route::post('/change-password', [AdminController::class, 'updatePassword'])->name('update.password');

        Route::group(['prefix' => 'customers', 'as' => 'customers.'], function () {
            Route::get('/', [CustomerController::class, 'index'])->name('index');
            Route::get('/add', [CustomerController::class, 'create'])->name('create');
            Route::post('/add', [CustomerController::class, 'store'])->name('store');
            Route::get('/{user:id}/detail', [CustomerController::class, 'show'])->name('show');
            Route::get('/{user:id}/verif', [CustomerController::class, 'verif'])->name('verif');
            Route::get('/{user:id}', [CustomerController::class, 'edit'])->name('edit');
            Route::get('/{anggota:id}/approved', [CustomerController::class, 'approved'])->name('approved');
            Route::get('/{anggota:id}/reject', [CustomerController::class, 'reject'])->name('reject');
            Route::put('/update/{user:id}', [CustomerController::class, 'update'])->name('update');
        });

        Route::group(['prefix' => 'pengguna', 'as' => 'pengguna.'], function () {
            Route::get('/', [PenggunaController::class, 'index'])->name('index');
            Route::get('/show/{id}', [PenggunaController::class, 'show'])->name('show');
            Route::post('/update', [PenggunaController::class, 'update'])->name('update');
        });

        Route::group(['prefix' => 'jenisPinjaman', 'as' => 'jenisPinjaman.'], function () {
            Route::get('/', [JenisPinjamanController::class, 'index'])->name('index');
            Route::get('/add', [JenisPinjamanController::class, 'create'])->name('create');
            Route::post('/add', [JenisPinjamanController::class, 'store'])->name('store');
            Route::get('/{jenis_pinjaman:id}/detail', [JenisPinjamanController::class, 'show'])->name('show');
            Route::get('/{jenis_pinjaman:id}', [JenisPinjamanController::class, 'edit'])->name('edit');
            Route::put('/update/{jenis_pinjaman:id}', [JenisPinjamanController::class, 'update'])->name('update');
        });

        Route::group(['prefix' => 'pengajuan', 'as' => 'pengajuan.'], function () {
            Route::get('/', [PengajuanPinjamanController::class, 'index'])->name('index');
            Route::get('/show/{id}', [PengajuanPinjamanController::class, 'show'])->name('show');
            Route::get('/approved/{pengajuan:id}', [PengajuanPinjamanController::class, 'approved'])->name('approved');
            Route::get('/reject/{pengajuan:id}', [PengajuanPinjamanController::class, 'reject'])->name('reject');
        });

        Route::group(['prefix' => 'pinjaman', 'as' => 'pinjaman.'], function () {
            Route::get('/', [PinjamanController::class, 'index'])->name('index');
            Route::get('/show/{id}', [PinjamanController::class, 'show'])->name('show');
            Route::get('/bayar/{id}', [PinjamanController::class, 'bayar'])->name('bayar');
        });

        Route::group(['prefix' => 'laporan', 'as' => 'laporan.'], function () {
            Route::get('/', [LaporanController::class, 'index'])->name('index');
            Route::post('/download', [LaporanController::class, 'download'])->name('download');
        });

        Route::group(['prefix' => 'pembayaran', 'as' => 'pembayaran.'], function () {
            Route::get('/', [PembayaranController::class, 'index'])->name('index');
            Route::get('/add', [PembayaranController::class, 'add'])->name('add');
            Route::get('/show/{id}', [PembayaranController::class, 'show'])->name('show');
            Route::get('/cetak/{id}', [PembayaranController::class, 'cetak'])->name('cetak');
            Route::post('/bayar', [PembayaranController::class, 'store'])->name('store');
        });
    });

    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/profile', [UserController::class, 'myProfile'])->name('myProfile');
        Route::get('/profile-edit', [UserController::class, 'editProfile'])->name('edit.profile');
        Route::put('/update', [UserController::class, 'update'])->name('update.profile');
        Route::get('/change-password', [UserController::class, 'changePassword'])->name('change.password');
        Route::post('/change-password', [UserController::class, 'updatePassword'])->name('update.password');

        Route::group(['prefix' => 'identity', 'as' => 'identity.'], function () {
            Route::get('/upload', [UserController::class, 'uploadIdentity'])->name('upload');
            Route::post('/upload', [UserController::class, 'uploadIdentityStore'])->name('store');
        });

        Route::group(['prefix' => 'pinjaman', 'as' => 'pinjaman.'], function () {
            Route::get('/', [UserPinjamanController::class, 'index'])->name('index');
            Route::get('/detail/{id}', [UserPinjamanController::class, 'detail'])->name('detail');
            Route::get('/ajukan/{jenis_pinjaman:id}', [UserPinjamanController::class, 'ajukan'])->name('ajukan');
        });

        Route::group(['prefix' => 'pengajuan', 'as' => 'pengajuan.'], function () {
            Route::get('/', [UserPengajuanController::class, 'index'])->name('index');
            Route::get('/detail/{id}', [UserPengajuanController::class, 'detail'])->name('detail');
            Route::get('/ajukan/{jenis_pinjaman:id}', [UserPengajuanController::class, 'ajukan'])->name('ajukan');
        });

        Route::group(['prefix' => 'pembayaran', 'as' => 'pembayaran.'], function () {
            Route::get('/', [UserPembayaranController::class, 'index'])->name('index');
            Route::get('/detail/{id}', [UserPembayaranController::class, 'detail'])->name('detail');
        });
    });
});
