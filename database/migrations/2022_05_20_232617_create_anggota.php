<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota', function (Blueprint $table) {
            $table->id();
            $table->string('nomor_anggota')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('nomor_ktp')->nullable();
            $table->string('file_ktp')->nullable();
            $table->integer('skore')->nullable();
            $table->string('pekerjaan')->nullable();
            $table->boolean('verified')->default(false);
            $table->timestamp('verified_at')->nullable();
            $table->integer('verified_by')->nullable();
            $table->timestamps();
        });

        Schema::create('jenis_pinjaman', function (Blueprint $table) {
            $table->id();
            $table->string('jenis_pinjaman')->nullable();
            $table->integer('jumlah')->nullable();
            $table->integer('biaya_layanan')->nullable();
            $table->integer('tenor')->nullable();
            $table->enum('jenis_tenor',['hari','minggu','bulan'])->default('hari');
            $table->integer('jumlah_angsur')->nullable();
            $table->integer('denda')->nullable();
            $table->integer('score_minimum')->nullable();
            $table->integer('angsuran')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });

        Schema::create('pengajuan', function (Blueprint $table) {
            $table->id();
            $table->integer('anggota_id')->nullable();
            $table->integer('jenis_pinjaman_id')->nullable();
            $table->integer('jumlah')->nullable();
            $table->integer('tenor')->nullable();
            $table->enum('jenis_tenor',['hari','minggu','bulan'])->default('hari');
            $table->integer('jumlah_angsur')->nullable();
            $table->date('tanggal_pengajuan')->nullable();
            $table->enum('status', ['disetujui','ditolak','menunggu persetujuan'])->default('menunggu persetujuan');
            $table->timestamps();
        });

        Schema::create('pinjaman', function (Blueprint $table) {
            $table->id();
            $table->integer('jenis_pinjaman_id')->nullable();
            $table->integer('pengajuan_id')->nullable();
            $table->integer('anggota_id')->nullable();
            $table->integer('jumlah_pinjaman')->nullable();
            $table->integer('biaya_layanan')->nullable();
            $table->date('jumlah_diterima')->nullable();
            $table->boolean('flag_lunas')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota');
        Schema::dropIfExists('jenis_pinjaman');
        Schema::dropIfExists('pengajuan');
        Schema::dropIfExists('pinjaman');
    }
};
