<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pinjaman', function (Blueprint $table) {
            $table->decimal('bunga')->nullable();
        });

        Schema::table('jenis_pinjaman', function (Blueprint $table) {
            $table->decimal('bunga')->nullable();
        });

        Schema::table('tagihan', function (Blueprint $table) {
            $table->integer('pokok')->nullable();
            $table->integer('bunga')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
