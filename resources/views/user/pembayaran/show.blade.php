@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div>
        <a href="{{route('admin.pengajuan.index')}}" class="btn btn-link"><i class="fa fa-arrow-left"></i> Kembali</a>
    </div>
    <div class="row">
        <div class="col-12 col-xl-8">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-md-8 d-flex align-items-center">
                            <h6 class="mb-0">Pembayaran Pinjaman # {{$pembayaran['invoice']}}</h6>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <hr class="horizontal gray-light my-4">
                    <ul class="list-group">
                        <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Nama Anggota:</strong> &nbsp; {{$pembayaran['tagihan']['pinjaman']['anggota']['user']['name']}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tanggal Pembayaran:</strong> &nbsp; {{ "Rp. ".number_format($pembayaran['angsuran'])}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Angsuran Ke:</strong> &nbsp; {{$pembayaran['tagihan']['angsuran_ke']}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Angsuran:</strong> &nbsp; {{ "Rp. " .number_format($pembayaran['angsuran'])}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Keterlambatan:</strong> &nbsp;{{$pembayaran['keterlambatan']}} </li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Denda:</strong> &nbsp; {{"Rp. " .number_format($pembayaran['denda'])}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Total:</strong> &nbsp; {{"Rp. " .number_format($pembayaran['total'])}}</li>
                    </ul>
                </div>
                 <a href="{{route('admin.pembayaran.cetak',$pembayaran['id'])}}" class="btn btn-sm btn-outline-info d-flex align-items-center justify-content-center">Cetak</a>
            </div>

        </div>

    </div>


    @include('templates.footer')
</div>
@endsection
