@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div>
        <a href="{{route('admin.pengajuan.index')}}" class="btn btn-link"><i class="fa fa-arrow-left"></i> Kembali</a>
    </div>
    <div class="row">
        <div class="col-md-6 mt-4">
            <div class="card">
                <div class="card-header pb-0 px-3">
                    <h6 class="mb-0">Informasi Pinjaman</h6>
                </div>
                <div class="card-body pt-4 p-3">
                    <ul class="list-group">
                        <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                            <div class="d-flex flex-column">
                                <h6 class="mb-3 text-sm">{{$pinjaman->anggota->user->name}}</h6>
                                <span class="mb-2 text-xs">Jumlah Pinjaman: <span class="text-dark font-weight-bold ms-sm-2">{{"Rp. ".number_format($pinjaman->jumlah_pinjaman)}}</span></span>
                                <span class="mb-2 text-xs">Biaya Layanan: <span class="text-dark ms-sm-2 font-weight-bold">{{"Rp. ".number_format($pinjaman->biaya_layanan)}}</span></span>
                                <span class="mb-2 text-xs">Tanggal Pinjaman: <span class="text-dark ms-sm-2 font-weight-bold">{{date('Y-m-d', strtotime($pinjaman->created_at))}}</span></span>
                                <span class="text-xs">Status:
                                    @if($pinjaman->flag_lunas=='1')
                                    <span class="text-dark ms-sm-2 font-weight-bold">Lunas</span>
                                    @else
                                    <span class="text-dark ms-sm-2 font-weight-bold">Belum Lunas</span>
                                    @endif

                                </span>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 mt-4">
            <div class="card h-100 mb-4">
                <div class="card-header pb-0 px-3">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="mb-0">Tagihan</h6>
                        </div>

                    </div>
                </div>
                <div class="card-body pt-4 p-3">

                    <ul class="list-group">
                        @foreach($tagihan as $row)
                        <li class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                            <div class="d-flex align-items-center">


                                <div class="d-flex flex-column">
                                    <h6 class="mb-1 text-dark text-sm">Angsuran Ke - {{$row->angsuran_ke}}</h6>
                                    <span class="text-xs">Jatuh Tempo {{$row->jatuh_tempo}}</span>
                                </div>
                            </div>
                            @if($row->lunas==0 && strtotime($row->jatuh_tempo) < strtotime(date('Y-m-d')) ) <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold">
                                {{"Rp. ".number_format($row->angsuran)}}
                </div>
                @else
                <div class="d-flex align-items-center text-info text-gradient text-sm font-weight-bold">
                    {{"Rp. ".number_format($row->angsuran)}}
                </div>
                @endif
                @if($row->lunas==1)

                <button class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check"></i></button>
                @endif

                </li>
                @endforeach
                </ul>

            </div>
        </div>
    </div>
</div>
<footer class="footer pt-3  ">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-6 mb-lg-0 mb-4">
                <div class="copyright text-center text-sm text-muted text-lg-start">
                    © <script>
                        document.write(new Date().getFullYear())

                    </script>,
                    made with <i class="fa fa-heart"></i> by
                    <a href="https://www.creative-tim.com" class="font-weight-bold" target="_blank">Creative Tim</a>
                    for a better web.
                </div>
            </div>
            <div class="col-lg-6">
                <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                    <li class="nav-item">
                        <a href="https://www.creative-tim.com" class="nav-link text-muted" target="_blank">Creative Tim</a>
                    </li>
                    <li class="nav-item">
                        <a href="https://www.creative-tim.com/presentation" class="nav-link text-muted" target="_blank">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="https://www.creative-tim.com/blog" class="nav-link text-muted" target="_blank">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a href="https://www.creative-tim.com/license" class="nav-link pe-0 text-muted" target="_blank">License</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</div>

@include('templates.footer')
</div>
@endsection
