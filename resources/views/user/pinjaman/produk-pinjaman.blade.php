@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">

        <div class="col-md-7 mt-4">
            <div class="card">
                <div class="card-header pb-0 px-3">
                    <h6 class="mb-0">Produk Untuk Anda</h6>
                </div>
                <div class="card-body pt-4 p-3">
                    <ul class="list-group">
                        @foreach($jenisPinjaman as $row)
                        <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                            <div class="d-flex flex-column">
                                <h6 class="mb-3 text-sm">{{$row->jenis_pinjaman}}</h6>
                                <span class="mb-2 text-xs">Jumlah Pinjaman: <span class="text-dark font-weight-bold ms-sm-2">{{"Rp." .number_format($row->jumlah)}}</span></span>
                                <span class="mb-2 text-xs">Jangka Waktu: <span class="text-dark ms-sm-2 font-weight-bold">{{$row->tenor}} - {{$row->jenis_tenor}}</span></span>
                                <span class="text-xs">Jumlah Angsur: <span class="text-dark ms-sm-2 font-weight-bold">{{$row->jumlah_angsur}} kali</span></span>
                                <span class="text-xs">Angsuran: <span class="text-dark ms-sm-2 font-weight-bold">{{"Rp. ".number_format($row->angsuran)}} </span></span>
                                <span class="text-xs">Biaya Layanan: <span class="text-dark ms-sm-2 font-weight-bold">{{"Rp. ".number_format($row->biaya_layanan)}} </span></span>
                                <span class="text-xs">Bunga: <span class="text-dark ms-sm-2 font-weight-bold">{{"Rp. ".number_format($row->jumlah * $row->bunga / 100)}} ({{number_format($row->bunga)}} %) </span></span>
                                <span class="text-xs">Jumlah Diterima: <span class="text-dark ms-sm-2 font-weight-bold">{{"Rp. ".number_format($row->jumlah - $row->biaya_layanan)}} </span></span>
                            </div>
                            <div class="ms-auto text-end">
                                <a href="{{route('users.pinjaman.ajukan', $row->id)}}" class="btn btn-info btn-sm px-2 py-2 text-xs">Ajukan</a>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

    </div>

    @include('templates.footer')
</div>
@endsection
