@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div>
        <a href="{{route('users.pinjaman.index')}}" class="btn btn-link"><i class="fa fa-arrow-left"></i> Kembali</a>
    </div>
    <div class="row">
        <div class="col-12 col-xl-8">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-md-8 d-flex align-items-center">
                            <h6 class="mb-0">Informasi Pengajuan</h6>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <hr class="horizontal gray-light my-4">
                    <ul class="list-group">
                        <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Produk:</strong> &nbsp; {{$pengajuan->jenis->jenis_pinjaman}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jumlah Pengajuan:</strong> &nbsp;{{"Rp. ".number_format($pengajuan->jumlah)}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tanggal Pengajuan:</strong> &nbsp;{{date('Y-m-d', strtotime($pengajuan->created_at))}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Biaya Layanan:</strong> &nbsp; {{"Rp. ".number_format($pengajuan->jenis->biaya_layanan)}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jumlah Diterima:</strong> &nbsp; {{"Rp. ".number_format($pengajuan->jumlah - $pengajuan->jenis->biaya_layanan)}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tenor:</strong> &nbsp; {{$pengajuan->tenor." ".$pengajuan->jenis_tenor}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jumlah Angsuran:</strong> &nbsp; {{$pengajuan->jumlah_angsuran}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Nilai Angsuran:</strong> &nbsp; {{"Rp. ".number_format($pengajuan->angsuran)}}</li>
                    </ul>
                </div>
            </div>
        </div>



        <div class="col-12 col-xl-4">
            <div class="card card-blog card-plain h-100">
                <button class="btn btn-success btn-sm px-2 py-2 text-xs">
                    {{$pengajuan->status}}
                </button>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
