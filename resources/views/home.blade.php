@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12 mt-4">
            <div class="card mb-4">
                <div class="card-header pb-0 p-3">
                    <h6 class="mb-1">Daftar Kamar Wisma</h6>
                    <p class="text-sm">Silakan pilih kamar dalam list ini</p>
                </div>
                <div class="card-body p-3">
                    <div class="row">
                        @foreach($public_buildings as $building)
                        <div class="col-xl-3 col-md-6 mb-xl-0 mb-4">
                            <div class="card card-blog card-plain">
                                <div class="position-relative">
                                    <a class="d-block shadow-xl border-radius-xl">
                                        <img src="{{asset($building->cover)}}" alt="img-blur-shadow" style="width: 100%; height: 200px" class="img-fluid shadow border-radius-xl">
                                    </a>
                                </div>
                                <div class="card-body px-1 pb-0">
                                    <p class="text-gradient text-dark mb-2 text-sm">{{$building->category->name}}</p>
                                    <a href="{{route('wisma.show', $building->uuid)}}">
                                        <h5>{{$building->name}}</h5>
                                    </a>
                                    <p class="text-dark text-sm font-weight-bold">Rp. {{number_format($building->price,0,',',',')}}</p>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <a href="{{route('wisma.show', $building->uuid)}}" class="btn btn-outline-secondary btn-sm mb-0">Detail</a>
                                        <a href="{{route('users.orders.create', $building->uuid)}}" class="btn btn-outline-primary btn-sm mb-0">Pesan</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
