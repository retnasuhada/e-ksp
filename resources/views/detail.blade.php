@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-md-7 mt-4">
            <div class="card">
                <div class="card-header pb-0 px-3 d-flex justify-content-between align-items-center">
                    <h6 class="mb-0">Detail Wisma</h6>
                    <a href="{{route('wisma.index')}}" class="btn btn-dark btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <div class="card-body pt-4 p-3">
                    <ul class="list-group">
                        <li class="list-group-item border-0 d-flex p-4 mb-2 mt-3 bg-gray-100 border-radius-lg">
                            <div class="d-flex flex-column">
                                <img src="{{asset($publicBuilding->cover)}}" class="img-fluid rounded" alt="">
                            </div>
                        </li>
                        <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                            <div class="d-flex flex-column">
                                <h6 class="mb-3 text-sm text-capitalize">{{$publicBuilding->name}}</h6>
                                <span class="mb-2 text-xs">Nama Wisma: <span class="text-dark font-weight-bold ms-sm-2 text-capitalize">{{$publicBuilding->name}}</span></span>
                                <span class="mb-2 text-xs">Nomor: <span class="text-dark ms-sm-2 font-weight-bold">{{$publicBuilding->public_building_number}}</span></span>
                                <span class="mb-2 text-xs">Harga: <span class="text-dark ms-sm-2 font-weight-bold">Rp. {{number_format($publicBuilding->price,0,',',',')}}</span></span>
                                <span class="text-xs">Status: <span class="text-dark ms-sm-2 font-weight-bold">{{$publicBuilding->status}}</span></span>
                            </div>
                        </li>
                        <li class="list-group-item border-0 d-flex p-4 mb-2 mt-3 bg-gray-100 border-radius-lg">
                            <div class="d-flex flex-column">
                                <h6 class="mb-3 text-sm">Kategori Wisma</h6>
                                <span class="text-xs">Kategori: <span class="text-dark font-weight-bold ms-sm-2">{{$publicBuilding->category->name ?? '-'}}<span></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-5 mt-4">
            <div class="card mb-4">
                <div class="card-header pb-0 px-3">
                    <div class="row">
                        <div class="col-md-12">
                            <h6 class="mb-0">Fasilitas</h6>
                        </div>
                    </div>
                </div>
                <div class="card-body pt-4 p-3">
                    <div class="card-text">{{$publicBuilding->facility->notes ?? '-'}}</div>
                </div>
            </div>
        </div>
    </div>
    @include('templates.footer')
</div>
@endsection
