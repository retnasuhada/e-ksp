 <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3" id="sidenav-main">
     <div class="sidenav-header">
         <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
         <a class="navbar-brand m-0" href="/home">
             <img src="{{asset('assets/img/logo.png')}}" class="navbar-brand-img h-100" alt="main_logo">
             <span class="ms-1 font-weight-bold">{{config('app.name')}}</span>
         </a>
     </div>
     <hr class="horizontal dark mt-0">
     <div class="collapse navbar-collapse w-auto" id="sidenav-collapse-main">
         <ul class="navbar-nav">
             @if(auth()->user()->role === 'superadmin' || auth()->user()->role === 'admin')
             <li class="nav-item">
                 <a class="nav-link {{request()->is('home*') ? 'active' : ''}}" href="{{route('home')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-dashboard {{request()->is('home*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Dashboard</span>
                 </a>
             </li>
             <li class="nav-item">
                 <a class="nav-link {{request()->is('admin/pengguna*') ? 'active' : ''}}" href="{{route('admin.pengguna.index')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-user {{request()->is('admin/pengguna*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Pengguna</span>
                 </a>
             </li>
             <li class="nav-item mt-3">
                 <h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Manajemen Data</h6>
             </li>
             <li class="nav-item">
                 <a class="nav-link {{request()->is('admin/customers*') ? 'active' : ''}}" href="{{route('admin.customers.index')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-users {{request()->is('admin/customers*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Customers</span>
                 </a>
             </li>



             <li class="nav-item">
                 <a class="nav-link {{request()->is('admin/jenisPinjaman*') ? 'active' : ''}}" href="{{route('admin.jenisPinjaman.index')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-folder {{request()->is('admin/customers*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Jenis Pinjaman</span>
                 </a>
             </li>

             <li class="nav-item">
                 <a class="nav-link {{request()->is('admin/pengajuan*') ? 'active' : ''}}" href="{{route('admin.pengajuan.index')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-folder {{request()->is('admin/customers*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Pengajuan Pinjaman</span>
                 </a>
             </li>

             <li class="nav-item">
                 <a class="nav-link {{request()->is('admin/pinjaman*') ? 'active' : ''}}" href="{{route('admin.pinjaman.index')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-folder {{request()->is('admin/customers*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Pinjaman</span>
                 </a>
             </li>

             <li class="nav-item">
                 <a class="nav-link {{request()->is('admin/pembayaran*') ? 'active' : ''}}" href="{{route('admin.pembayaran.index')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-folder {{request()->is('admin/customers*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Pembayaran</span>
                 </a>
             </li>

             <li class="nav-item">
                 <a class="nav-link {{request()->is('admin/laporan*') ? 'active' : ''}}" href="{{route('admin.laporan.index')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-folder {{request()->is('admin/laporan*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Laporan</span>
                 </a>
             </li>

             @endif
             @if(auth()->user()->role === 'customer')
             <li class="nav-item mt-3">
                 <h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">General</h6>
             </li>
             <li class="nav-item">
                 <a class="nav-link {{request()->is('users/profile*') ? 'active' : ''}}" href="{{route('users.myProfile')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-user {{request()->is('users/profile*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Profil</span>
                 </a>
             </li>

             <li class="nav-item">
                 <a class="nav-link {{request()->is('users/pengajuan*') ? 'active' : ''}}" href="{{route('users.pengajuan.index')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-book {{request()->is('users/pengajuan*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Pengajuan</span>
                 </a>
             </li>
             <li class="nav-item">
                 <a class="nav-link {{request()->is('users/pinjaman*') ? 'active' : ''}}" href="{{route('users.pinjaman.index')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-book {{request()->is('users/pinjaman*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Pinjaman</span>
                 </a>
             </li>

             <li class="nav-item">
                 <a class="nav-link {{request()->is('users/pembayaran*') ? 'active' : ''}}" href="{{route('users.pembayaran.index')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-book {{request()->is('users/pembayaran*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Pembayaran</span>
                 </a>
             </li>





             <li class="nav-item">
                 <a class="nav-link {{request()->is('users/change-password*') ? 'active' : ''}}" href="{{route('users.change.password')}}">
                     <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                         <i class="fa fa-lock {{request()->is('users/change-password*') ? 'text-white' : 'text-secondary'}} cursor-pointer"></i>
                     </div>
                     <span class="nav-link-text ms-1">Ubah Password</span>
                 </a>
             </li>
             @endif
         </ul>
     </div>
 </aside>
