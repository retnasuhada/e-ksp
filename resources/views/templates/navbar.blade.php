<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="/home">Dashboard</a></li>
            </ol>
            <h6 class="font-weight-bolder mb-0">{{config('app.name')}}</h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
            <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            </div>
            <ul class="navbar-nav justify-content-end">
                <li class="nav-item dropdown pe-2 d-flex align-items-center">
                    <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-user cursor-pointer"></i>
                    </a>
                    <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
                        @if(auth()->user()->role === 'admin' || auth()->user()->role === 'superadmin')
                        <li class="mb-2">
                            <a class="dropdown-item border-radius-md" href="{{route('admin.myProfile')}}">
                                <div class="d-flex py-1">
                                    <div class="my-auto avatar avatar-sm bg-gradient-dark me-3">
                                        <i class="fa fa-user cursor-pointer"></i>
                                    </div>
                                    <div class="d-flex flex-column justify-content-center">
                                        <h6 class="text-sm font-weight-normal mb-1">Profil</h6>

                                        <p class="text-xs text-secondary mb-0 ">
                                            <i class="fa fa-check me-1"></i>
                                            {{auth()->user()->name}}
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class="dropdown-item border-radius-md" href="{{route('admin.change.password')}}">
                                <div class="d-flex py-1">
                                    <div class="my-auto avatar avatar-sm bg-gradient-dark me-3">
                                        <i class="fa fa-lock cursor-pointer"></i>
                                    </div>
                                    <div class="d-flex flex-column justify-content-center">
                                        <h6 class="text-sm font-weight-normal mb-1">Ubah Password</h6>
                                    </div>
                                </div>
                            </a>
                        </li>
                        @else
                        <li class="mb-2">
                            <a class="dropdown-item border-radius-md" href="{{route('users.myProfile')}}">
                                <div class="d-flex py-1">
                                    <div class="my-auto avatar avatar-sm bg-gradient-dark me-3">
                                        <i class="fa fa-user cursor-pointer"></i>
                                    </div>
                                    <div class="d-flex flex-column justify-content-center">
                                        <h6 class="text-sm font-weight-normal mb-1">Profil</h6>

                                        <p class="text-xs text-secondary mb-0 ">
                                            <i class="fa fa-check me-1"></i>
                                            {{auth()->user()->name}}
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="mb-2">
                            <a class="dropdown-item border-radius-md" href="{{route('users.change.password')}}">
                                <div class="d-flex py-1">
                                    <div class="my-auto avatar avatar-sm bg-gradient-dark me-3">
                                        <i class="fa fa-lock cursor-pointer"></i>
                                    </div>
                                    <div class="d-flex flex-column justify-content-center">
                                        <h6 class="text-sm font-weight-normal mb-1">Ubah Password</h6>
                                    </div>
                                </div>
                            </a>
                        </li>
                        @endif

                        <li>
                            <a class="dropdown-item border-radius-md" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <div class="d-flex py-1">
                                    <div class="avatar avatar-sm bg-gradient-secondary me-3 my-auto">
                                        <i class="fa fa-sign-out cursor-pointer"></i>
                                    </div>
                                    <div class="d-flex flex-column justify-content-center">
                                        <h6 class="text-sm font-weight-normal mb-1">Logout</h6>
                                    </div>
                                </div>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
