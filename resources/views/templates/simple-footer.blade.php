<footer class="footer py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center mb-4 mt-2">
                <a href="javascript:;" target="_blank" class="text-secondary me-xl-4 me-4">
                    <span class="text-lg fab fa-facebook"></span>
                </a>
                <a href="javascript:;" target="_blank" class="text-secondary me-xl-4 me-4">
                    <span class="text-lg fab fa-twitter"></span>
                </a>
                <a href="javascript:;" target="_blank" class="text-secondary me-xl-4 me-4">
                    <span class="text-lg fab fa-instagram"></span>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-8 mx-auto text-center mt-1">
                <a href="tel:082275963992" class="mb-0 text-success text-bold d-block">Telp : 0822-7596-3992</a>
                <a href="#" target="_blank" class="mb-0 text-secondary text-bold">KSP RAJAWALI CITRA MANDIRI, Ruko Permata Regency</a>
                </br>
                <a href="mailto:sabarpanjaitan@gmail.com" class="mb-0 text-secondary text-bold">Email : sabarpanjaitan@gmail.com</a>
            </div>
        </div>
        <div class="row">
            <div class="col-8 mx-auto text-center mt-1">
                <p class="mb-0 text-secondary">
                    Copyright © {{date("Y")}} {{config('app.name')}}
                </p>
            </div>
        </div>
    </div>
</footer>
