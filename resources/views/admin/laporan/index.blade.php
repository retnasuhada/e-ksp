@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Menu Laporan</h6>
                    <a href="{{route('admin.jenisPinjaman.index')}}" class="btn btn-dark btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('admin.laporan.download')}}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                @csrf
                                <div class="form-group">
                                    <label for="jenis_pinjaman">Jenis Laporan</label>
                                    <select name="jenis_laporan" class="form-control">
                                    <option value="1">Pengajuan</option>
                                    <option value="2">Pinjaman</option>
                                    <option value="3">Pembayaran</option>
                                    <option value="4">Anggota</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                @csrf
                                <div class="form-group">
                                    <label for="jenis_pinjaman">Start</label>
                                    <input type="date" name="start" class="form-control" required>

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                @csrf
                                <div class="form-group">
                                    <label for="jenis_pinjaman">End</label>
                                    <input type="date" name="end" class="form-control" required>
                                </div>
                            </div>


                            <div class="text-center">
                                <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Download</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
