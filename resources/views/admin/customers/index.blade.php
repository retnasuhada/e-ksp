@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Customers</h6>
                    <a href="{{route('admin.customers.create')}}" class="btn btn-dark"><i class="fa fa-plus-circle"></i> Tambah</a>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7">#</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Name</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Email</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Phone</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Status</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Register At</th>
                                    <th class="text-center text-uppercase text-dark text-sm font-weight-bolder opacity-7">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td class="align-middle text-center text-sm">
                                        <span class="badge badge-sm bg-gradient-success">{{$loop->iteration}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-sm text-dark">{{$user->name}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-sm text-dark">{{$user->email}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm">{{$user->phone ?? '-'}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        @if($user->anggota)
                                        @if($user->anggota->verified == 1)
                                        <span class="badge badge-sm bg-gradient-success">Actice</span>
                                        @elseif($user->anggota->verified == 0)
                                        <span class="badge badge-sm bg-gradient-danger">Nonactive</span>
                                        @endif
                                        @else
                                        <span class="badge badge-sm bg-gradient-danger">Nonactive</span>
                                        @endif
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm">{{ date('Y-m-d', strtotime($user->created_at)) ?? '-'}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <div class="d-flex justify-content-center align-items-center gap-1">
                                            <a href="{{route('admin.customers.edit', $user->id)}}" class="btn btn-info btn-sm px-2 py-2 text-xs">
                                                Ubah
                                            </a>
                                            <a href="{{route('admin.customers.show', $user->id)}}" class="btn btn-danger btn-sm px-2 py-2 text-xs">
                                                Detail
                                            </a>
                                            @if($user->anggota)
                                            @if($user->anggota->verified == 0)
                                            <a href="{{route('admin.customers.verif', $user->id)}}" class="btn btn-success btn-sm px-2 py-2 text-xs">
                                                Verifikasi
                                            </a>
                                            @endif
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
