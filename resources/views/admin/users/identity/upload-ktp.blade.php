@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Upload KTP</h6>
                    <a href="{{route('users.myProfile')}}" class="btn btn-dark btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4"></div>
                        <div class="col-sm-12 col-md-8 col-lg-8">
                            <form method="POST" action="{{route('users.identity.store')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="ktp_number">No KTP</label>
                                    <input type="number" name="ktp_number" class="form-control @error('ktp_number') is-invalid @enderror" placeholder="No KTP">
                                    @error('ktp_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="ktp_file">Foto KTP</label>
                                    <input type="file" name="ktp_file" class="form-control @error('ktp_file') is-invalid @enderror">
                                    @error('ktp_file')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="mt-2 text-xs">File berupa : PNG,JPG,JPEG maksimal 1MB</span>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
