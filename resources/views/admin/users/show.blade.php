@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Detail / Edit Pengguna</h6>
                    <a href="{{route('admin.pengguna.index')}}" class="btn btn-dark btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('admin.pengguna.update')}}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                @csrf
                                <input name="id" type="hidden" value="{{$user->id}}">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control"  value="{{$user->name}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="jenis_pinjaman">Email</label>
                                    <input type="text" name="jenis_pinjaman" class="form-control" disabled  value="{{$user->email}}">
                                </div>



                                <div class="form-group">
                                    <label for="jenis_pinjaman">Phone</label>
                                    <input type="text" name="jenis_pinjaman" class="form-control"  disabled value="{{$user->phone}}">
                                </div>

                                <div class="form-group">
                                    <label for="role">Level</label>
                                    <select name="role" id="role" class="form-control">
                                        <option value="customer" <?php if($user->role=='customer'){echo "selected";} ?>>customer</option>
                                        <option value="admin" <?php if($user->role=='admin'){echo "selected";} ?> >admin</option>
                                        <option value="superadmin" <?php if($user->role=='superadmin'){echo "selected";} ?> >super admin</option>
                                    </select>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
