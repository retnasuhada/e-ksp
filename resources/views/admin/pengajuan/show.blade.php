@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div>
        <a href="{{route('admin.pengajuan.index')}}" class="btn btn-link"><i class="fa fa-arrow-left"></i> Kembali</a>
    </div>
    <div class="row">
        <div class="col-12 col-xl-4">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-md-8 d-flex align-items-center">
                            <h6 class="mb-0">Informasi Pengajuan</h6>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <hr class="horizontal gray-light my-4">
                    <ul class="list-group">
                        <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Produk:</strong> &nbsp; {{$pengajuan->jenis->jenis_pinjaman}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jumlah Pengajuan:</strong> &nbsp;{{"Rp. ".number_format($pengajuan->jumlah)}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Biaya Layanan:</strong> &nbsp; {{"Rp. ".number_format($pengajuan->jenis->biaya_layanan)}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jumlah Diterima:</strong> &nbsp; {{"Rp. ".number_format($pengajuan->jumlah - $pengajuan->jenis->biaya_layanan)}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tenor:</strong> &nbsp; {{$pengajuan->tenor." ".$pengajuan->jenis_tenor}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jumlah Angsuran:</strong> &nbsp; {{$pengajuan->jumlah_angsuran}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Nilai Angsuran:</strong> &nbsp; {{"Rp. ".number_format($pengajuan->angsuran)}}</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-12 col-xl-4">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-md-8 d-flex align-items-center">
                            <h6 class="mb-0">Informasi Anggota</h6>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <hr class="horizontal gray-light my-4">
                    <ul class="list-group">
                        <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Nama:</strong> &nbsp; {{$pengajuan->anggota->user->name}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Email:</strong> &nbsp;{{$pengajuan->anggota->user->email}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Alamat:</strong> &nbsp; {{$pengajuan->anggota->user->address}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jenis Kelamin:</strong> &nbsp; {{$pengajuan->anggota->user->gender}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Phone:</strong> &nbsp; {{$pengajuan->anggota->user->phone}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Pekerjaan:</strong> &nbsp; {{$pengajuan->anggota->pekerjaan}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Skor Kredit:</strong> &nbsp; {{$pengajuan->anggota->skore}}</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-12 col-xl-4">
            <div class="card card-blog card-plain h-100">
                <div class="position-relative">

                    @if($pengajuan->anggota->file_ktp)
                    <a class="d-block shadow-xl border-radius-xl">
                        <img src="{{asset('assets/img/ktp/'.$pengajuan->anggota->file_ktp)}}" alt="img-blur-shadow" class="img-fluid shadow border-radius-xl">
                    </a>
                    @else
                    <a class="d-block shadow-xl border-radius-xl">
                        <img src="{{asset('assets/img/curved-images/white-curved.jpg')}}" alt="img-blur-shadow" class="img-fluid shadow border-radius-xl">
                    </a>
                    @endif
                </div>
                <div class="card-body px-1 pb-0">
                    <p class="text-gradient text-dark mb-2 text-sm">KTP FILE</p>
                    <p class="text-gradient text-dark mb-2 text-sm">Nomor KTP</p>
                    <a href="javascript:;">
                        <h5>
                            {{$pengajuan->anggota->nomor_ktp}}
                        </h5>
                    </a>

                </div>
                @if($pengajuan->status=='menunggu persetujuan')
                <a href="{{route('admin.pengajuan.approved', $pengajuan->id)}}" class="btn btn-success btn-sm px-2 py-2 text-xs">
                    Setujui
                </a>
                <a href="{{route('admin.pengajuan.reject', $pengajuan->id)}}" class="btn btn-danger btn-sm px-2 py-2 text-xs">
                    Tolak
                </a>

                @endif
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
