@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Daftar Pengajuan</h6>
                    {{-- <a href="{{route('admin.jenisPinjaman.create')}}" class="btn btn-dark"><i class="fa fa-plus-circle"></i> Tambah</a> --}}
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive">
                        <table class="table align-items-center">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7">#</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Tanggal</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Nama</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Jenis Pinjaman</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Jumlah</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Status</th>
                                    <th class="text-center text-uppercase text-dark text-sm font-weight-bolder opacity-7">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($data as $row)
                                <tr>
                                    <td class="align-middle text-center text-sm">
                                        <span class="badge badge-sm bg-gradient-success">{{$loop->iteration}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-sm text-dark">{{$row->tanggal_pengajuan}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-sm text-dark">{{$row->anggota->user->name}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm">{{$row->jenis->jenis_pinjaman}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm">{{"Rp. " .number_format($row->jumlah)}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm">{{$row->status}}</span>
                                    </td>

                                    <td class="align-middle text-center">
                                        <div class="d-flex justify-content-center align-items-center gap-1">
                                            <a href="{{route('admin.pengajuan.show', $row->id)}}" class="btn btn-info btn-sm px-2 py-2 text-xs">
                                                Detail
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {!! $data->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
