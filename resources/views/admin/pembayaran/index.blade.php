@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Daftar Pembayaran</h6>
                    {{-- <a href="{{route('admin.pembayaran.add')}}" class="btn btn-dark"><i class="fa fa-plus-circle"></i> Tambah</a> --}}
                    <form method="GET" action="{{route('admin.pinjaman.index')}}" enctype="multipart/form-data">
                        <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                            <div class="input-group">
                                <span class="input-group-text text-body"><i class="fas fa-search" aria-hidden="true"></i></span>
                                <input type="text" name="search" id="search" class="form-control" placeholder="Type here...">
                            </div>
                        </div>
                    </form>

                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive">
                        <table class="table align-items-center">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7">#</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Tanggal</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Nama</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Angsuran</th>

                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Angsuran Ke</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Keterlambatan</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Denda</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Total</th>
                                    <th class="text-center text-uppercase text-dark text-sm font-weight-bolder opacity-7">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($data as $row)
                                <tr>
                                    <td class="align-middle text-center text-sm">
                                        <span class="badge badge-sm bg-gradient-success">{{$loop->iteration}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-sm text-dark">{{$row->tanggal}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-sm text-dark">{{$row->name}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm">{{"Rp. " .number_format($row->angsuran)}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm">{{$row->angsuran_ke}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm">{{$row->keterlambatan}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm">{{"Rp. " .number_format($row->denda)}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm">{{"Rp. " .number_format($row->denda + $row->angsuran)}}</span>
                                    </td>

                                    <td class="align-middle text-center">
                                        <div class="d-flex justify-content-center align-items-center gap-1">
                                            <a href="{{route('admin.pembayaran.show', $row->id)}}" class="btn btn-info btn-sm px-2 py-2 text-xs">
                                                Detail
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {!! $data->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>

@endsection
