@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Form Pembayaran Tagihan</h6>
                    <a href="{{route('admin.pinjaman.show', $data['tagihan']['pinjaman_id'])}}" class="btn btn-dark btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('admin.pembayaran.store')}}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                @csrf
                                <div class="form-group">
                                    <label for="nama_anggota">Nama</label>
                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{$data['tagihan']['pinjaman']['anggota']['user']['name']}}">
                                    <input type="hidden" name="tagihan_id" value="{{$data['tagihan']['id']}}">

                                </div>
                                <div class="form-group">
                                    <label for="jumlah">Jumlah Pinjaman</label>
                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{ "Rp." .number_format($data['tagihan']['pinjaman']['jumlah_pinjaman'])}}">
                                </div>



                                <div class="form-group">
                                    <label for="tenor">Tanggal Pinjaman</label>
                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{ date("Y-m-d", strtotime($data['tagihan']['pinjaman']['created_at']))}}">
                                </div>
                                <div class="form-group">
                                    <label for="tenor">Biaya Layanan</label>
                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{ "Rp." .number_format($data['tagihan']['pinjaman']['biaya_layanan'])}}">
                                </div>
                                <div class="form-group">
                                    <label for="tenor">Tenor</label>
                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{$data['tagihan']['pinjaman']['pengajuan']['tenor']."-".$data['tagihan']['pinjaman']['pengajuan']['jenis_tenor']}}">
                                </div>

                                <div class="form-group">
                                    <label for="tenor">Jumlah Kali Angsur</label>
                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{ $data['tagihan']['pinjaman']['pengajuan']['jumlah_angsur']}} kali">
                                </div>

                            </div>

                            <div class="col-sm-12 col-md-6 col-lg-6">

                                <div class="form-group">
                                    <label for="tenor">Angsuran</label>
                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{ "Rp." .number_format($data['tagihan']['pinjaman']['pengajuan']['angsuran'])}}">
                                </div>
                                <div class="form-group">
                                    <label for="tenor">Pembayaran Ke-</label>
                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{ $data['tagihan']['angsuran_ke']}}">
                                </div>
                                <div class="form-group">
                                    <label for="tenor">Jatuh Tempo</label>
                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{ date("Y-m-d", strtotime($data['tagihan']['jatuh_tempo']))}}">
                                </div>
                                <div class="form-group">
                                    <label for="tenor">Keterlambatan</label>

                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{ $data['keterlambatan']. "-Hari"}}">
                                </div>
                                <div class="form-group">
                                    <label for="tenor">Denda Keterlambatan</label>

                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{"Rp. " .number_format($data['denda'])}}">
                                </div>
                                <div class="form-group">
                                    <label for="tenor">Total</label>

                                    <input type="text" name="nama_anggota" class="form-control" disabled value="{{"Rp. " .number_format($data['total'])}}">
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Bayar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
