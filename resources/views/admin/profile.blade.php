@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12 col-xl-4">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-md-8 d-flex align-items-center">
                            <h6 class="mb-0">Profil</h6>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <hr class="horizontal gray-light my-4">
                    <ul class="list-group">
                        <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Nama:</strong> &nbsp; {{$user->name}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Email:</strong> &nbsp;{{$user->email}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">No Telepon:</strong> &nbsp; {{$user->phone}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jenis Kelamin:</strong> &nbsp; {{$user->gender}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tanggal Lahir:</strong> &nbsp; {{$user->dob}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Alamat:</strong> &nbsp; {{$user->address}}</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-12 col-xl-4">
            <div class="card h-100">
                <div class="card-header pb-0 p-3 d-flex align-items-center justify-content-between">
                    <h6 class="mb-0">Profil</h6>
                    <a href="{{route('users.edit.profile')}}" class="btn btn-link"><i class="fa fa-edit"></i> Edit</a>
                </div>
                <div class="card-body p-3">
                    <ul class="list-group">
                        <li class="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                            @if($user->avatar == null)
                            <div class="avatar me-3">
                                <img src="{{asset('assets/img/kal-visuals-square.jpg')}}" alt="kal" class="border-radius-lg shadow">
                            </div>
                            @else
                            <div class="avatar me-3">
                                <img src="{{asset('assets/img/avatar/'.$user->avatar)}}" alt="kal" class="border-radius-lg shadow">
                            </div>
                            @endif
                            <div class="d-flex align-items-start flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{$user->name}}</h6>
                                <p class="mb-0 text-xs">{{$user->email}}</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-12 col-xl-4">
            <div class="card card-blog card-plain h-100">
                <div class="position-relative">
                    @if($anggota)
                    @if($anggota->file_ktp)
                    <a class="d-block shadow-xl border-radius-xl">
                        <img src="{{asset('assets/img/ktp/'.$anggota->file_ktp)}}" alt="img-blur-shadow" class="img-fluid shadow border-radius-xl">
                    </a>
                    @else
                    <a class="d-block shadow-xl border-radius-xl">
                        <img src="{{asset('assets/img/curved-images/white-curved.jpg')}}" alt="img-blur-shadow" class="img-fluid shadow border-radius-xl">
                    </a>
                    @endif
                    @endif
                </div>
                <div class="card-body px-1 pb-0">
                    <p class="text-gradient text-dark mb-2 text-sm">KTP FILE</p>
                    <p class="text-gradient text-dark mb-2 text-sm">Nomor KTP</p>
                    <a href="javascript:;">
                        <h5>
                            {{$anggota ? $anggota->nomor_ktp : ''}}
                        </h5>
                    </a>
                    <div class="d-flex align-items-center justify-content-start">
                        <a href="{{route('users.identity.upload')}}" class="btn btn-outline-primary btn-sm mb-0">Edit KTP</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
