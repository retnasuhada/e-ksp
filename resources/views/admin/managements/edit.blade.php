@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Edit Admin</h6>
                    <a href="{{route('admin.managements.index')}}" class="btn btn-dark btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-lg-8">
                            <form method="POST" action="{{route('admin.managements.update', $user->id)}}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="name">Nama</label>
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nama" value="{{old('name', $user->name)}}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="phone">No Telepon</label>
                                    <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" placeholder="No Telepon" value="{{old('phone', $user->phone)}}">
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="dob">Tanggal Lahir</label>
                                    <input type="date" name="dob" class="form-control @error('dob') is-invalid @enderror" value="{{old('dob', $user->dob)}}">
                                    @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="gender">Jenis Kelamin</label>
                                    <select name="gender" id="gender" class="form-control @error('gender') is-invalid @enderror">
                                        <option value="">Pilih Jenis Kelamin</option>
                                        <option value="L" {{$user->gender == 'L' ? 'selected' : ''}}>Laki-Laki</option>
                                        <option value="P" {{$user->gender == 'P' ? 'selected' : ''}}>Perempuan</option>
                                    </select>
                                    @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="address">Alamat</label>
                                    <textarea type="text" name="address" class="form-control @error('address') is-invalid @enderror" placeholder="Alamat lengkap">{{old('address', $user->address)}}</textarea>
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                @if($user->avatar !== null)
                                <div class="avatar me-3">
                                    <img src="{{asset($user->avatar)}}" alt="kal" class="border-radius-lg shadow">
                                </div>
                                @endif

                                <div class="form-group">
                                    <label for="avatar">Foto</label>
                                    <input type="file" name="avatar" class="form-control @error('avatar') is-invalid @enderror">
                                    @error('avatar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="mt-2 text-xs">File berupa : PNG,JPG,JPEG atau GIF maksimal 1MB</span>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 hidden"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
