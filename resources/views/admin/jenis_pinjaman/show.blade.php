@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div>
        <a href="{{route('admin.jenisPinjaman.index')}}" class="btn btn-link"><i class="fa fa-arrow-left"></i> Kembali</a>
    </div>
    <div class="row">
        <div class="col-12 col-xl-12">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-md-8 d-flex align-items-center">
                            <h6 class="mb-0">Detail</h6>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <hr class="horizontal gray-light my-4">
                    <ul class="list-group">
                        <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Jenis Pinjaman:</strong> &nbsp; {{$data->jenis_pinjaman}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Limit Pinjaman:</strong> &nbsp;{{"Rp. " .number_format($data->jumlah)}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Biaya Layanan:</strong> &nbsp; {{"Rp. " .number_format($data->biaya_layanan)}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Bunga:</strong> &nbsp; {{"Rp. " .number_format($data->jumlah * $data->bunga/100 )}}({{number_format($data->bunga)}} %)</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tenor:</strong> &nbsp; {{$data->tenor}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jenis Tenor:</strong> &nbsp; {{$data->jenis_tenor}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jumlah Kali Angsur:</strong> &nbsp; {{$data->jumlah_angsur}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Angsuran:</strong> &nbsp; {{"Rp. " .number_format($data->angsuran)}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Denda Keterlambatan:</strong> &nbsp; {{$data->denda. "%"}}</li>
                        <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Skor Minimal:</strong> &nbsp; {{$data->score_minimum}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
