@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Tambah Jenis Pinjaman</h6>
                    <a href="{{route('admin.jenisPinjaman.index')}}" class="btn btn-dark btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('admin.jenisPinjaman.store')}}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                @csrf
                                <div class="form-group">
                                    <label for="jenis_pinjaman">Jenis Pinjaman</label>
                                    <input type="text" name="jenis_pinjaman" class="form-control @error('jenis_pinjaman') is-invalid @enderror" placeholder="Jenis Pinjaman" value="{{old('jenis_pinjaman')}}">
                                    @error('jenis_pinjaman')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="jumlah">Limit Pinjaman</label>
                                    <input type="number" name="jumlah" class="form-control @error('jumlah') is-invalid @enderror" placeholder="Limit Pinjaman" value="{{old('jumlah')}}">
                                    @error('jumlah')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>



                                <div class="form-group">
                                    <label for="tenor">Tenor</label>
                                    <input type="number" name="tenor" class="form-control @error('tenor') is-invalid @enderror" placeholder="Tenor" value="{{old('tenor')}}">
                                    @error('tenor')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="jenis_tenor">Jenis Tenor</label>
                                    <select name="jenis_tenor" id="jenis_tenor" class="form-control @error('jenis_tenor') is-invalid @enderror">
                                        <option value="">Pilih Jenis Tenor</option>
                                        <option value="hari" >Hari</option>
                                        <option value="minggu" >Minggu</option>
                                        <option value="bulan" >Bulan</option>
                                    </select>
                                    @error('jenis_tenor')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>



                            </div>

                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="jumlah_angsur">Jumlah Angsur</label>
                                    <input type="number" name="jumlah_angsur" class="form-control @error('jumlah_angsur') is-invalid @enderror" placeholder="Jumlah Angsur" value="{{old('jumlah_angsur')}}">
                                    @error('jumlah_angsur')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="biaya_layanan">Biaya Layanan</label>
                                    <input type="number" name="biaya_layanan" class="form-control @error('biaya_layanan') is-invalid @enderror" placeholder="Biaya Layanan" value="{{old('biaya_layanan')}}">
                                    @error('biaya_layanan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="denda">Denda Keterlambatan  (%)</label>
                                    <input type="number" name="denda" class="form-control @error('denda') is-invalid @enderror" placeholder="Denda Keterlambatan" value="{{old('denda')}}">
                                    @error('denda')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="score_minimum">Skor Minimum</label>
                                    <input type="number" name="score_minimum" class="form-control @error('score_minimum') is-invalid @enderror" placeholder="Skor Minimum" value="{{old('score_minimum')}}">
                                    @error('score_minimum')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
