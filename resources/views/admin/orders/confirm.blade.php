@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Konfirmasi Pesanan</h6>
                    <a href="{{route('admin.orders.index')}}" class="btn btn-dark btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <p class="text-left font-weight-bold">Lengkapi form berikut untuk melanjutkan proses booking Wisma</p>
                            <div class="border border-dark"></div>
                            <h4 class="mt-2">{{$order->public_building_name}}</h4>
                            <p class="mt-2">Total yang dibayar, Rp. {{number_format($order->amount,2,',',',')}}</p>
                            <div class="border border-dark"></div>
                            <h4 class="mt-2 text-lg">Dibayar Oleh:</h4>
                            <p>Atas Nama: {{$order->paid_by}}</p>
                            <div>
                                <a href="{{asset($order->paid_file)}}" target="_blank">
                                    <img src="{{asset($order->paid_file)}}" width="100px" height="100px" alt="Bukti bayar">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8 col-lg-8">
                            <form method="POST" action="{{route('admin.orders.confirm', $order->order_number)}}">
                                @csrf
                                <div class="form-group">
                                    <label for="remark">Catatan</label>
                                    <textarea name="remark" class="form-control @error('remark') is-invalid @enderror" placeholder="Catatan">{{old('remark')}}</textarea>
                                    @error('remark')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Konfirmasi</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
