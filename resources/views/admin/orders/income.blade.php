@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Pendapatan</h6>
                    <a href="{{route('admin.incomes.export')}}" target="_blank" class="btn btn-danger"><i class="fa fa-print"></i> Print</a>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7">#</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">No Pesanan</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Nama Wisma</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Tanggal Dibayar</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Status</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Dipesan Pada</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td class="align-middle text-center text-sm">
                                        <span class="badge badge-sm bg-gradient-success">{{$loop->iteration}}</span>
                                    </td>
                                    <td>
                                        <p class="text-sm text-dark mb-0">#{{$order->order_number ?? '-'}}</p>
                                    </td>
                                    <td>
                                        <div class="avatar me-3">
                                            <img src="{{asset($order->public_building_cover)}}" alt="cover" class="border-radius-lg shadow">
                                        </div>
                                        <p class="text-xs text-dark mb-0">{{$order->public_building_name}}</p>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm font-weight-bold">{{$order->paid_at ?? '-'}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm font-weight-bold">{{$order->last_status}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-xs font-weight-bold">{{$order->created_at}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm font-weight-bold">Rp. {{ number_format($order->amount,2,',',',')}}</span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6"><strong>Total Pendapatan</strong></td>
                                    <td colspan="1"><strong>Rp. {{ number_format($totalIncome,2,',','.') }}</strong> </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
