@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Booking Wisma</h6>
                    <a href="{{route('admin.orders.index')}}" class="btn btn-dark btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <p class="text-left font-weight-bold">Lengkapi form berikut untuk melanjutkan proses booking Wisma</p>
                        </div>

                        <div class="col-sm-12 col-md-8 col-lg-8">
                            <form method="POST" action="{{route('admin.orders.store')}}">
                                @csrf
                                <div class="form-group">
                                    <label for="public_building_id">Wisma</label>
                                    <select name="public_building_id" id="public_building_id" class="form-control @error('public_building_id') is-invalid @enderror">
                                        <option value="">Pilih Wisma</option>
                                        @foreach($publicBuildings as $building)
                                        <option value="{{$building->id}}">{{$building->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('public_building_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="check_in_at">Check In</label>
                                            <input type="date" name="check_in_at" class="form-control @error('check_in_at') is-invalid @enderror">
                                            @error('check_in_at')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="check_out_at">Check Out</label>
                                            <input type="date" name="check_out_at" class="form-control @error('check_out_at') is-invalid @enderror">
                                            @error('check_out_at')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="name">Nama Pelanggan</label>
                                            <input type="text" name="name" placeholder="Nama Pelanggan" class="form-control @error('name') is-invalid @enderror">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="phone_number">No Telepon</label>
                                            <input type="text" name="phone_number" class="form-control @error('phone_number') is-invalid @enderror">
                                            @error('phone_number')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="nik">NIK</label>
                                            <input type="text" name="nik" placeholder="Masukkan NIK" class="form-control @error('nik') is-invalid @enderror">
                                            @error('nik')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="email">No Telepon</label>
                                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Buat Pesanan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
