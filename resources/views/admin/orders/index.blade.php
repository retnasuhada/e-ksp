@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0 d-flex justify-content-between align-items-center">
                    <h6>Orders</h6>
                    @if(auth()->user()->role == 'admin')
                    <a href="{{route('admin.orders.create')}}" class="btn btn-dark"><i class="fa fa-plus-circle"></i> Tambah</a>
                    @endif
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7">#</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">No Pesanan</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Nama Wisma</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Total</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Tanggal Dibayar</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Status</th>
                                    <th class="text-uppercase text-dark text-sm font-weight-bolder opacity-7 ps-2">Dipesan Pada</th>
                                    <th class="text-center text-uppercase text-dark text-sm font-weight-bolder opacity-7">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td class="align-middle text-center text-sm">
                                        <span class="badge badge-sm bg-gradient-success">{{$loop->iteration}}</span>
                                    </td>
                                    <td>
                                        <p class="text-sm text-dark mb-0">#{{$order->order_number ?? '-'}}</p>
                                    </td>
                                    <td>
                                        <div class="avatar me-3">
                                            <img src="{{asset($order->public_building_cover)}}" alt="cover" class="border-radius-lg shadow">
                                        </div>
                                        <p class="text-xs text-dark mb-0">{{$order->public_building_name}}</p>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm font-weight-bold">Rp. {{ number_format($order->amount,2,',',',')}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm font-weight-bold">{{$order->paid_at ?? '-'}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-sm font-weight-bold">{{$order->last_status}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <span class="text-dark text-xs font-weight-bold">{{$order->created_at}}</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <div class="d-flex justify-content-center align-items-center gap-1">
                                            <a href="{{route('admin.orders.show', $order->order_number)}}" class="btn btn-info btn-sm px-2 py-2 text-xs">Lihat</a>

                                            @if($order->paid_at && !$order->confirmed_at)
                                            <a href="{{route('admin.orders.confirm', $order->order_number)}}" class="btn btn-light btn-sm px-2 py-2 text-xs">Konfirmasi</a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="text-center">
                        {{$orders->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
