<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name')}}</title>
    <style>
        .table {
            --bs-table-bg: transparent;
            --bs-table-accent-bg: transparent;
            --bs-table-striped-color: #67748e;
            --bs-table-striped-bg: rgba(0, 0, 0, 0.05);
            --bs-table-active-color: #67748e;
            --bs-table-active-bg: rgba(0, 0, 0, 0.1);
            --bs-table-hover-color: #67748e;
            --bs-table-hover-bg: rgba(0, 0, 0, 0.075);
            width: 100%;
            margin-bottom: 1rem;
            color: #67748e;
            vertical-align: top;
            border-color: #e9ecef;
        }

        .table> :not(caption)>*>* {
            padding: 0.5rem 0.5rem;
            background-color: var(--bs-table-bg);
            border-bottom-width: 1px;
            box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
        }

        .table>tbody {
            vertical-align: inherit;
        }

        .table>thead {
            vertical-align: bottom;
        }

        .table> :not(:first-child) {
            border-top: 2px solid currentColor;
        }

        .table-bordered> :not(caption)>* {
            border-width: 1px 0;
        }

        .table-bordered> :not(caption)>*>* {
            border-width: 0 1px;
        }

        .table {
            border-collapse: inherit;
        }

        .table thead th {
            padding: 0.75rem 1.5rem;
            text-transform: capitalize;
            letter-spacing: 0px;
            border-bottom: 1px solid #e9ecef;
        }

        .table th {
            font-weight: 600;
            font-size: 13px;
        }

        .table td,
        .table th {
            white-space: nowrap;
            font-size: 10px;
        }

        .table.align-items-center td,
        .table.align-items-center th {
            vertical-align: middle;
        }

        .table tbody tr:last-child td {
            border-width: 0;
        }

        .table> :not(:last-child)> :last-child>* {
            border-bottom-color: #e9ecef;
        }
    </style>
</head>

<body>
    <div class="text-center my-5">
        <h1 class="text-center">{{config('app.name')}}</h1>
        <h4>{{config('app.name')}} | Pendapatan</h4>
        <p>{{ date('Y-m-d H:i:s') }}</p>
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>No Pesanan</th>
                <th>Nama Wisma</th>
                <th>Tanggal Dibayar</th>
                <th>Status</th>
                <th>Dipesan Pada</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
            <tr>
                <td>
                    <span>{{$loop->iteration}}</span>
                </td>
                <td>
                    <p>#{{$order->order_number ?? '-'}}</p>
                </td>
                <td>
                    <p>{{$order->public_building_name}}</p>
                </td>
                <td>
                    <span>{{$order->paid_at ?? '-'}}</span>
                </td>
                <td>
                    <span>{{$order->last_status}}</span>
                </td>
                <td>
                    <span>{{$order->created_at}}</span>
                </td>
                <td>
                    <span>Rp. {{ number_format($order->amount,2,',',',')}}</span>
                </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6"><strong>Total Pendapatan</strong></td>
                <td colspan="1"><strong>Rp. {{ number_format($totalIncome,2,',','.') }}</strong> </td>
            </tr>
        </tfoot>
    </table>
</body>

</html>
