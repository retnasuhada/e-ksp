@extends('templates.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row my-4">
        <div class="col-lg-8 col-md-6 mb-md-0 mb-4">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="col-lg-6 col-7">
                            <h6>Order Information</h6>
                        </div>
                    </div>
                </div>
                <div class="card-body pb-2">
                    <ul class="list-group">
                        <li class="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                            <div class="avatar me-3">
                                <img src="{{asset($order->public_building_cover)}}" alt="cover" class="border-radius-lg shadow">
                            </div>
                            <div class="d-flex align-items-start flex-column justify-content-center">
                                <h6 class="mb-0 text-lg">{{$order->public_building_name}}</h6>
                                <p class="mb-0 text-xs">Rp. {{number_format($order->public_building_price,0,',',',')}}</p>
                            </div>
                        </li>
                        <li class="list-group-item border-0 d-flex align-items-center px-0">
                            <div class="d-flex align-items-start flex-column justify-content-center">
                                <p class="mb-0 text-sm text-dark">Check In: {{date('Y-m-d', strtotime($order->check_in_at))}}</p>
                            </div>

                            <p class="pe-3 ps-0 mb-0 ms-auto text-sm text-dark">Check Out: {{date('Y-m-d', strtotime($order->check_out_at))}}</p>
                        </li>
                        <li class="list-group-item border-0 d-flex align-items-center px-0">
                            <div class="d-flex align-items-start flex-column justify-content-center">
                                <p class="mb-0 text-sm text-dark">Lama Menginap: {{$order->until}} Malam</p>
                            </div>
                        </li>
                        <li class="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                            <div class="d-flex align-items-start flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{$order->order_number}}</h6>
                                <p class="mb-0 text-xs">Dipesan pada: {{$order->created_at}}</p>
                            </div>
                            <p class="btn btn-link pe-3 ps-0 mb-0 ms-auto text-lg">Rp. {{ number_format($order->amount,2,',',',')}}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="card h-100">
                <div class="card-header pb-0">
                    <h6>Orders Status</h6>
                    <p class="text-sm">
                        <span class="font-weight-bold">Disini anda bisa melihat detail status pesanan</span>
                    </p>
                </div>

                <div class="card-body p-3">
                    <div class="timeline timeline-one-side">
                        @foreach($order->statuses as $status)
                        <div class="timeline-block mb-3">
                            <span class="timeline-step">
                                <i class="fa fa-check text-success text-gradient"></i>
                            </span>
                            <div class="timeline-content">
                                <h6 class="text-dark text-sm font-weight-bold mb-0">{{$status->name}}</h6>
                                <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">{{$status->created_at}}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-8 col-md-6 mb-md-0 mb-4 mt-4">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="row">
                        <div class="col-lg-6 col-7">
                            <h6>Informasi Pembayaran</h6>
                        </div>
                    </div>
                </div>
                <div class="card-body pb-2">
                    @if($order->paid_at)
                    <ul class="list-group">
                        <li class="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                            <div class="avatar me-3">
                                <img src="{{asset($order->paid_file)}}" alt="cover" class="border-radius-lg shadow">
                            </div>
                            <div class="d-flex align-items-start flex-column justify-content-center">
                                <h6 class="mb-0 text-lg">{{$order->paid_at ?? '-'}}</h6>
                                <p class="mb-0 text-sm">Atas Nama: {{$order->paid_by ?? '-'}}</p>
                            </div>
                        </li>
                    </ul>
                    @else
                    <div class="alert alert-warning text-white" role="alert">
                        <span>Pesanan belum dibayar</span>
                    </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 mt-4">
            <div class="card h-100">
                <div class="card-header pb-0">
                    <h6>Orders Log</h6>
                    <p class="text-sm">
                        <span class="font-weight-bold">Disini anda bisa melihat detail log pesanan</span>
                    </p>
                </div>

                <div class="card-body p-3">
                    <div class="timeline timeline-one-side">
                        @foreach($order->logs as $log)
                        <div class="timeline-block mb-3">
                            <span class="timeline-step">
                                <i class="fa fa-check-circle text-success text-gradient"></i>
                            </span>
                            <div class="timeline-content">
                                <h6 class="text-dark text-sm font-weight-bold mb-0">{{$log->action}}</h6>
                                <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">{{$log->comment}}</p>
                                <p class="text-secondary font-weight-bold text-xs mt-1 mb-0">{{$log->created_at}}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('templates.footer')
</div>
@endsection
